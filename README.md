# API Server for "IAM" platform

### Live demo: https://ompeciam.musicapply.ru


### Project features  
   * :heavy_check_mark: Spring Boot  
   * :heavy_check_mark: Hibernate  
   * :heavy_check_mark: Validation  
   * :heavy_check_mark: Advanced filtering and pagination (Criteria API)  
   * :heavy_check_mark: JWT Auth   
   * :heavy_check_mark: Mapstruct   

