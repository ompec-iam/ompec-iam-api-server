package ru.ompec.iam.util;

import org.hibernate.validator.HibernateValidator;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import javax.validation.ConstraintViolation;
import java.util.Locale;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class BaseValidationTest {

    protected static LocalValidatorFactoryBean validator;

    @BeforeAll
    public static void setupValidatorInstance() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:messages");
        messageSource.setDefaultEncoding("UTF-8");

        validator = new LocalValidatorFactoryBean();
        validator.setProviderClass(HibernateValidator.class);
        validator.setValidationMessageSource(messageSource);
        validator.afterPropertiesSet();
        Locale.setDefault(Locale.forLanguageTag("ru"));
    }

    protected <T> Set<String> constraintViolationsToStringSet(Set<ConstraintViolation<T>> violations) {
        return violations.stream().map(ConstraintViolation::getMessage).collect(Collectors.toSet());
    }

}
