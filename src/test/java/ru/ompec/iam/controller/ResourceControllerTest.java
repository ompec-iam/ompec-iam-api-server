package ru.ompec.iam.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import ru.ompec.iam.domain.Resource;
import ru.ompec.iam.payload.resource.CreateResourceRequest;
import ru.ompec.iam.service.ResourceService;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Sql(value = {"/create-sql-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/create-sql-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@ActiveProfiles("test")
public class ResourceControllerTest {

    @MockBean
    private ResourceService resourceService;

    private final MockMvc mockMvc;
    private final ResourceController resourceController;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public ResourceControllerTest(MockMvc mockMvc,
                                  ResourceController resourceController) {
        this.mockMvc = mockMvc;
        this.resourceController = resourceController;
    }

    @Test
    public void contextLoad() throws Exception {
        assertThat(mockMvc).isNotNull();
        assertThat(resourceController).isNotNull();
        assertThat(resourceService).isNotNull();
    }

    @Test
    public void authenticatedRequestTest() throws Exception {
        this.mockMvc.perform(post("/api/resource"))
                    .andDo(print())
                    .andExpect(status().isUnauthorized());
    }

    @WithUserDetails("anya")
    @Test
    public void testCreateResource() throws Exception {
        String resourceTitle = "Moodle";

        CreateResourceRequest request = new CreateResourceRequest(resourceTitle);
        String content = objectMapper.writeValueAsString(request);

        Resource resource = Resource.builder()
                                    .title(resourceTitle)
                                    .id(UUID.randomUUID())
                                    .build();
        when(resourceService.createResource(any())).thenReturn(resource);

        this.mockMvc.perform(post("/api/resource").content(content).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isCreated())
                    .andExpect(jsonPath("id", is(resource.getId().toString())))
                    .andExpect(jsonPath("title", is(resourceTitle)));
    }

    @WithUserDetails("kirill")
    @Test
    public void testCreateResourceNotGranted() throws Exception {
        String resourceTitle = "Moodle";

        CreateResourceRequest request = new CreateResourceRequest(resourceTitle);
        String content = objectMapper.writeValueAsString(request);

        Resource resource = Resource.builder()
                                    .title(resourceTitle)
                                    .id(UUID.randomUUID())
                                    .build();
        when(resourceService.createResource(any())).thenReturn(resource);

        this.mockMvc.perform(post("/api/resource").content(content).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isForbidden());
    }

}
