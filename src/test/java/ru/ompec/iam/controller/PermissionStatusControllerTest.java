package ru.ompec.iam.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import ru.ompec.iam.domain.PermissionStatus;
import ru.ompec.iam.exception.DuplicateResourceException;
import ru.ompec.iam.payload.permissionstatus.CreatePermissionStatusRequest;
import ru.ompec.iam.service.PermissionStatusService;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Sql(value = {"/create-sql-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/create-sql-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@ActiveProfiles("test")
public class PermissionStatusControllerTest {

    @MockBean
    private PermissionStatusService permissionStatusService;

    private final MockMvc mockMvc;
    private final PermissionStatusController controller;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public PermissionStatusControllerTest(MockMvc mockMvc,
                                          PermissionStatusController controller) {
        this.mockMvc = mockMvc;
        this.controller = controller;
    }

    @Test
    public void contextLoad() {
        assertThat(mockMvc).isNotNull();
        assertThat(controller).isNotNull();
    }

    @Test
    public void testAuthenticatedRequests() throws Exception {
        this.mockMvc.perform(post("/api/permission_status"))
                    .andDo(print())
                    .andExpect(status().isUnauthorized());
    }

    @WithUserDetails(value = "anya")
    @Test
    public void testCreatePermissionStatusByAdmin() throws Exception {
        CreatePermissionStatusRequest request = new CreatePermissionStatusRequest("Active", "#aaa");
        PermissionStatus status = PermissionStatus.builder()
                                                  .id(UUID.randomUUID())
                                                  .status("Active")
                                                  .color("#aaa")
                                                  .build();
        String content = objectMapper.writeValueAsString(request);

        when(permissionStatusService.createPermissionStatus(any())).thenReturn(status);

        this.mockMvc.perform(post("/api/permission_status").content(content).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(header().string("location", "/api/permission_status/" + status.getId()))
                    .andExpect(status().isCreated())
                    .andExpect(jsonPath("id", is(status.getId().toString())))
                    .andExpect(jsonPath("color", is(status.getColor())))
                    .andExpect(jsonPath("status", is(status.getStatus())));
    }

    @WithUserDetails(value = "kirill")
    @Test
    public void testCreatePermissionStatusByUser() throws Exception {
        CreatePermissionStatusRequest request = new CreatePermissionStatusRequest("Active", "#aaa");
        PermissionStatus status = PermissionStatus.builder()
                                                  .id(UUID.randomUUID())
                                                  .status("Active")
                                                  .color("#aaa")
                                                  .build();
        String content = objectMapper.writeValueAsString(request);

        when(permissionStatusService.createPermissionStatus(any())).thenReturn(status);

        this.mockMvc.perform(post("/api/permission_status").content(content).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isForbidden())
                    .andExpect(jsonPath("message", is("Access Denied")));
    }

    @WithUserDetails(value = "anya")
    @Test
    public void testCreatePermissionDuplicateStatus() throws Exception {
        CreatePermissionStatusRequest request = new CreatePermissionStatusRequest("Active", "#aaa");
        PermissionStatus status = PermissionStatus.builder()
                                                  .id(UUID.randomUUID())
                                                  .status("Active")
                                                  .color("#aaa")
                                                  .build();
        String content = objectMapper.writeValueAsString(request);

        when(permissionStatusService.createPermissionStatus(any())).thenThrow(new DuplicateResourceException("PermissionStatus", "status", status.getStatus()));

        this.mockMvc.perform(post("/api/permission_status?lang=en").content(content).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isConflict());
        // not working - always apply ru-locale
                    //.andExpect(jsonPath("message", is("PermissionStatus already exist with status : " + status.getStatus())));
    }

}
