package ru.ompec.iam.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import ru.ompec.iam.domain.*;
import ru.ompec.iam.exception.ResourceNotFoundException;
import ru.ompec.iam.payload.employee.CreateEmployeeRequest;
import ru.ompec.iam.service.EmployeeService;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Sql(value = {"/create-sql-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/create-sql-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@ActiveProfiles("test")
public class EmployeeControllerTest {

    @MockBean
    private EmployeeService employeeService;

    private final MockMvc mockMvc;
    private final EmployeeController employeeController;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public EmployeeControllerTest(MockMvc mockMvc,
                                  EmployeeController employeeController) {
        this.mockMvc = mockMvc;
        this.employeeController = employeeController;
    }

    @Test
    public void contextLoad() {
        assertThat(mockMvc).isNotNull();
        assertThat(employeeController).isNotNull();
    }

    @Test
    public void testAuthenticatedRequests() throws Exception {
        this.mockMvc.perform(post("/api/employee").contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isUnauthorized());
        this.mockMvc.perform(get("/api/employee/" + UUID.randomUUID().toString()).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isUnauthorized());
        this.mockMvc.perform(delete("/api/employee/" + UUID.randomUUID().toString()).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isUnauthorized());
    }

    @WithUserDetails(value = "anya")
    @Test
    public void testCreateEmployeeByAdmin() throws Exception {
        CreateEmployeeRequest request = new CreateEmployeeRequest(
                "Анна",
                "Кулакова",
                "Ивановна",
                "Бухгалтер",
                "88008008800",
                UUID.randomUUID(),
                UUID.randomUUID()
        );

        UUID employeeId = UUID.randomUUID();
        Employee employee = Employee.builder().id(employeeId)
                                    .firstName("Анна")
                                    .lastName("Кулакова")
                                    .patronymic("Ивановна")
                                    .position("Бухгалтер")
                                    .phoneNumber("88008008800")
                                    .build();
        UUID departmentId = UUID.randomUUID();
        Department department = Department.builder().id(departmentId).title("Moodle").build();
        employee.setDepartment(department);

        when(employeeService.createEmployee(any())).thenReturn(employee);

        String content = objectMapper.writeValueAsString(request);

        this.mockMvc.perform(post("/api/employee").content(content).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(header().string("location", "/api/employee/" + employeeId.toString()))
                    .andExpect(status().isCreated())
                    .andExpect(jsonPath("id", is(employeeId.toString())))
                    .andExpect(jsonPath("firstName", is("Анна")))
                    .andExpect(jsonPath("lastName", is("Кулакова")))
                    .andExpect(jsonPath("patronymic", is("Ивановна")))
                    .andExpect(jsonPath("position", is("Бухгалтер")))
                    .andExpect(jsonPath("phoneNumber", is("88008008800")))
                    .andExpect(jsonPath("department.id", is(departmentId.toString())))
                    .andExpect(jsonPath("department.title", is(department.getTitle())));
    }

    @WithUserDetails(value = "kirill")
    @Test
    public void testCreateEmployeeByUser() throws Exception {
        CreateEmployeeRequest request = new CreateEmployeeRequest(
                "Анна",
                "Кулакова",
                "Ивановна",
                "Бухгалтер",
                "88008008800",
                UUID.randomUUID(),
                UUID.randomUUID()
        );

        UUID employeeId = UUID.randomUUID();
        Employee employee = Employee.builder().id(employeeId)
                                    .firstName("Анна")
                                    .lastName("Кулакова")
                                    .patronymic("Ивановна")
                                    .position("Бухгалтер")
                                    .phoneNumber("88008008800")
                                    .build();
        UUID departmentId = UUID.randomUUID();
        Department department = Department.builder().id(departmentId).title("Moodle").build();
        employee.setDepartment(department);

        when(employeeService.createEmployee(any())).thenReturn(employee);

        String content = objectMapper.writeValueAsString(request);

        this.mockMvc.perform(post("/api/employee?lang=ru").content(content).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isCreated());
    }

    @WithUserDetails(value = "anya")
    @Test
    public void testCreateEmployeeByAdminRequiredFields() throws Exception {
        CreateEmployeeRequest request = new CreateEmployeeRequest(
                "Анна",
                "Кулакова",
                null,
                "Бухгалтер",
                null,
                UUID.randomUUID(),
                UUID.randomUUID()
        );

        UUID employeeId = UUID.randomUUID();
        Employee employee = Employee.builder().id(employeeId)
                                    .firstName("Анна")
                                    .lastName("Кулакова")
                                    .position("Бухгалтер")
                                    .build();
        UUID departmentId = UUID.randomUUID();
        Department department = Department.builder().id(departmentId).title("Moodle").build();
        employee.setDepartment(department);

        when(employeeService.createEmployee(any())).thenReturn(employee);

        String content = objectMapper.writeValueAsString(request);

        this.mockMvc.perform(post("/api/employee").content(content).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(header().string("location", "/api/employee/" + employeeId.toString()))
                    .andExpect(status().isCreated())
                    .andExpect(jsonPath("id", is(employeeId.toString())))
                    .andExpect(jsonPath("firstName", is("Анна")))
                    .andExpect(jsonPath("lastName", is("Кулакова")))
                    .andExpect(jsonPath("patronymic").doesNotExist())
                    .andExpect(jsonPath("position", is("Бухгалтер")))
                    .andExpect(jsonPath("phoneNumber").doesNotExist())
                    .andExpect(jsonPath("department.id", is(departmentId.toString())))
                    .andExpect(jsonPath("department.title", is(department.getTitle())));
    }

    @WithUserDetails(value = "anya")
    @Test
    public void testGetEmployeeById() throws Exception {
        Department department = new Department();
        department.setId(UUID.randomUUID());
        department.setTitle("Info");

        Employee employee = Employee.builder()
                                    .id(UUID.randomUUID())
                                    .firstName("Анна")
                                    .lastName("Кулакова")
                                    .patronymic("Ивановна")
                                    .position("Бухгалтер")
                                    .phoneNumber("88008008800")
                                    .build();
        employee.setDepartment(department);

        PermissionStatus status = new PermissionStatus();
        status.setStatus("Active");
        status.setId(UUID.randomUUID());

        Resource resource = new Resource();
        resource.setTitle("Moodle");
        resource.setId(UUID.randomUUID());

        String login = "anita_shlyapa";
        String password = "mRT.8uKo_";

        Credential credential = new Credential();
        credential.setId(UUID.randomUUID());
        credential.setLogin(login);
        credential.setPassword(password);

        Permission permission = new Permission();
        permission.setId(UUID.randomUUID());
        permission.setStatus(status);
        permission.setResource(resource);
        permission.setEmployee(employee);
        permission.setCredential(credential);

        employee.getPermissions().add(permission);

        assertAll(
                () -> assertNotNull(employee.getId()),
                () -> assertNotNull(department.getId()),
                () -> assertNotNull(status.getId()),
                () -> assertNotNull(resource.getId()),
                () -> assertNotNull(credential.getId()),
                () -> assertNotNull(permission.getId())
        );

        when(employeeService.getEmployeeById(any())).thenReturn(employee);

        this.mockMvc.perform(get("/api/employee/" + employee.getId().toString()).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("id", is(employee.getId().toString())))
                    .andExpect(jsonPath("firstName", is(employee.getFirstName())))
                    .andExpect(jsonPath("lastName", is(employee.getLastName())))
                    .andExpect(jsonPath("patronymic", is(employee.getPatronymic())))
                    .andExpect(jsonPath("position", is(employee.getPosition())))
                    .andExpect(jsonPath("phoneNumber", is(employee.getPhoneNumber())))
                    .andExpect(jsonPath("department.id", is(employee.getDepartment().getId().toString())))
                    .andExpect(jsonPath("department.title", is(employee.getDepartment().getTitle())))
                    .andExpect(jsonPath("permissions").isArray())
                    .andExpect(jsonPath("permissions", hasSize(1)));
    }

    @WithUserDetails(value = "anya")
    @Test
    public void testEmployeeNotFoundById() throws Exception {
        UUID notFoundId = UUID.randomUUID();
        when(employeeService.getEmployeeById(any())).thenThrow(new ResourceNotFoundException("Employee", "id", notFoundId));

        this.mockMvc.perform(get("/api/employee/" + notFoundId.toString()).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isNotFound());
    }

    @WithUserDetails(value = "anya")
    @Test
    public void testRemoveEmployee() throws Exception {
        UUID employeeId = UUID.randomUUID();

        this.mockMvc.perform(delete("/api/employee/" + employeeId.toString()).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isNoContent());
    }

    @WithUserDetails(value = "kirill")
    public void testRemoveEmployeeAccessDenied() throws Exception {
        UUID employeeId = UUID.randomUUID();

        this.mockMvc.perform(delete("/api/employee/" + employeeId.toString()).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isForbidden());
    }

}