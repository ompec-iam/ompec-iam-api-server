package ru.ompec.iam.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import ru.ompec.iam.domain.Department;
import ru.ompec.iam.payload.department.CreateDepartmentRequest;
import ru.ompec.iam.service.DepartmentService;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Sql(value = {"/create-sql-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/create-sql-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@ActiveProfiles("test")
public class DepartmentControllerTest {

    @MockBean
    private DepartmentService departmentService;

    private final MockMvc mockMvc;
    private final DepartmentController departmentController;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public DepartmentControllerTest(MockMvc mockMvc,
                                    DepartmentController departmentController) {
        this.mockMvc = mockMvc;
        this.departmentController = departmentController;
    }

    @Test
    public void contextLoad() {
        assertThat(mockMvc).isNotNull();
        assertThat(departmentController).isNotNull();
    }

    @Test
    public void authenticatedRequestTest() throws Exception {
        this.mockMvc.perform(post("/api/department"))
                    .andDo(print())
                    .andExpect(status().isUnauthorized());
        this.mockMvc.perform(get("/api/department/" + UUID.randomUUID().toString()))
                    .andDo(print())
                    .andExpect(status().isUnauthorized());
        this.mockMvc.perform(get("/api/departments"))
                    .andDo(print())
                    .andExpect(status().isUnauthorized());
        this.mockMvc.perform(put("/api/department"))
                    .andDo(print())
                    .andExpect(status().isUnauthorized());
    }

    @WithUserDetails(value = "anya")
    @Test
    public void createDepartment() throws Exception {
        String departmentTitle = "Info";

        CreateDepartmentRequest request = new CreateDepartmentRequest(departmentTitle);
        String content = objectMapper.writeValueAsString(request);

        Department department = Department.builder()
                                          .id(UUID.randomUUID())
                                          .title(departmentTitle)
                                          .build();
        when(departmentService.createDepartment(any(CreateDepartmentRequest.class))).thenReturn(department);

        this.mockMvc.perform(post("/api/department").content(content).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().isCreated())
                    .andExpect(jsonPath("id", is(department.getId().toString())))
                    .andExpect(jsonPath("title", is(departmentTitle)));
    }

    @WithUserDetails(value = "kirill")
    @Test
    public void createDepartmentNotGranted() throws Exception {
        String departmentTitle = "Info";

        CreateDepartmentRequest request = new CreateDepartmentRequest(departmentTitle);
        String content = objectMapper.writeValueAsString(request);

        Department department = Department.builder()
                                          .id(UUID.randomUUID())
                                          .title(departmentTitle)
                                          .build();
        when(departmentService.createDepartment(any(CreateDepartmentRequest.class))).thenReturn(department);

        this.mockMvc.perform(post("/api/department").content(content).contentType(MediaType.APPLICATION_JSON))
                    .andDo(print())
                    .andExpect(status().is4xxClientError());
    }

}
