package ru.ompec.iam.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import ru.ompec.iam.domain.*;
import ru.ompec.iam.payload.permission.CreatePermissionRequest;
import ru.ompec.iam.service.PermissionService;


import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Sql(value = {"/create-sql-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/create-sql-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@ActiveProfiles("test")
public class PermissionControllerTest {

    @MockBean
    private PermissionService permissionService;

    private final MockMvc mockMvc;
    private final PermissionController permissionController;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public PermissionControllerTest(MockMvc mockMvc,
                                    PermissionController permissionController) {
        this.mockMvc = mockMvc;
        this.permissionController = permissionController;
    }

    @Test
    public void contextLoad() {
        assertThat(mockMvc).isNotNull();
        assertThat(permissionController).isNotNull();
    }

    @Test
    public void testAuthenticatedRequests() throws Exception {
        this.mockMvc.perform(post("/api/permission"))
                    .andDo(print())
                    .andExpect(status().isUnauthorized());
    }

    @WithUserDetails(value = "anya")
    @Test
    public void testCreatePermissionByAdmin() throws Exception {
        PermissionStatus status = new PermissionStatus();
        status.setStatus("Active");
        status.setId(UUID.randomUUID());

        Resource resource = new Resource();
        resource.setTitle("Moodle");
        resource.setId(UUID.randomUUID());

        Credential credential = new Credential();
        credential.setLogin("kirill_anatoly");
        credential.setPassword("mRT.8uKo_");
        credential.setId(UUID.randomUUID());

        Employee employee = Employee.builder()
                                    .id(UUID.randomUUID())
                                    .firstName("Анна")
                                    .lastName("Кулакова")
                                    .patronymic("Ивановна")
                                    .position("Бухгалтер")
                                    .phoneNumber("88008008800")
                                    .build();

        Permission permission = new Permission();
        permission.setStatus(status);
        permission.setCredential(credential);
        permission.setEmployee(employee);
        permission.setResource(resource);
        permission.setId(UUID.randomUUID());

        CreatePermissionRequest request = new CreatePermissionRequest(
                status.getId(),
                resource.getId(),
                employee.getId(),
                credential.getLogin(),
                credential.getPassword()
        );

        String body = objectMapper.writeValueAsString(request);

        when(permissionService.createPermission(any())).thenReturn(permission);

        this.mockMvc.perform(post("/api/permission").contentType(MediaType.APPLICATION_JSON).content(body))
                    .andDo(print())
                    .andExpect(status().isCreated())
                    .andExpect(header().string("location", "/api/permission/" + permission.getId()))
                    .andExpect(jsonPath("expirationDate").doesNotExist())
                    .andExpect(jsonPath("status.id", is(status.getId().toString())))
                    .andExpect(jsonPath("status.status", is(status.getStatus())))
                    .andExpect(jsonPath("resource.id", is(resource.getId().toString())))
                    .andExpect(jsonPath("resource.title", is(resource.getTitle())))
                    .andExpect(jsonPath("employeeId", is(employee.getId().toString())))
                    .andExpect(jsonPath("credential.id", is(credential.getId().toString())))
                    .andExpect(jsonPath("credential.login", is(credential.getLogin())))
                    .andExpect(jsonPath("credential.password", is(credential.getPassword())));
    }

}
