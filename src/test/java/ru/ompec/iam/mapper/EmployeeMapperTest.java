package ru.ompec.iam.mapper;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.ompec.iam.domain.Department;
import ru.ompec.iam.domain.Employee;
import ru.ompec.iam.domain.EmployeeStatus;
import ru.ompec.iam.payload.employee.CreateEmployeeRequest;
import ru.ompec.iam.repository.DepartmentRepository;
import ru.ompec.iam.repository.EmployeeStatusRepository;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class EmployeeMapperTest {

    @MockBean
    private DepartmentRepository departmentRepository;
    @MockBean
    private EmployeeStatusRepository statusRepository;

    @Autowired
    private EmployeeMapper mapper;

    @Test
    public void contextLoad() {
        assertThat(departmentRepository).isNotNull();
        assertThat(statusRepository).isNotNull();
        assertThat(mapper).isNotNull();
    }

    @Test
    public void testMapToEntity() throws Exception {
        UUID departmentId = UUID.fromString("7bad0611-15d0-4d63-9462-756ce083d10a");
        UUID statusId = UUID.fromString("7b0d0611-15d0-4d63-9462-756ce083d10a");
        CreateEmployeeRequest request = new CreateEmployeeRequest(
                "Anatoly",
                "Upyachev",
                "Alexandrovich",
                "Developer",
                "88008008800",
                departmentId,
                statusId
        );

        EmployeeStatus status = EmployeeStatus.builder()
                                              .id(statusId)
                                              .status("Active")
                                              .color("#FFFFFFFF")
                                              .build();

        Department department = Department.builder()
                                          .id(departmentId)
                                          .title("Moodle")
                                          .build();

        when(departmentRepository.findById(any())).thenReturn(Optional.of(department));
        when(statusRepository.findByIdFull(any())).thenReturn(Optional.of(status));

        Employee employee = mapper.toEntity(request);
        assertAll(
                () -> assertEquals(request.getFirstName(), employee.getFirstName()),
                () -> assertEquals(request.getLastName(), employee.getLastName()),
                () -> assertEquals(request.getPatronymic(), employee.getPatronymic()),
                () -> assertEquals(request.getPosition(), employee.getPosition()),
                () -> assertEquals(request.getPhoneNumber(), employee.getPhoneNumber()),
                () -> assertEquals(request.getDepartmentId(), employee.getDepartment().getId()),
                () -> assertEquals(request.getEmployeeStatusId(), employee.getStatus().getId()),
                () -> assertNull(employee.getId())
        );
    }

}
