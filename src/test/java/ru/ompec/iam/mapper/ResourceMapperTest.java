package ru.ompec.iam.mapper;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.ompec.iam.domain.Resource;
import ru.ompec.iam.payload.resource.CreateResourceRequest;
import ru.ompec.iam.payload.resource.ResourceResponse;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class ResourceMapperTest {

    private final ResourceMapper mapper;

    @Autowired
    public ResourceMapperTest(ResourceMapper mapper) {
        this.mapper = mapper;
    }

    @Test
    public void contextLoad() {
        assertThat(mapper).isNotNull();
    }

    @Test
    public void testMapCreateResourceRequestToResource() {
        CreateResourceRequest request = new CreateResourceRequest("Moodle");

        Resource resource = mapper.createResourceRequestToResource(request);

        assertAll(
                () -> assertEquals(request.getTitle(), request.getTitle()),
                () -> assertThat(resource.getPermissions()).isEmpty(),
                () -> assertThat(resource.getId()).isNull()
        );
    }

    @Test
    public void testMapResourceToResponse() {
        Resource resource = Resource.builder()
                                    .id(UUID.randomUUID())
                                    .title("Moodle")
                                    .build();

        ResourceResponse response = mapper.toResponse(resource);

        assertAll(
                () -> assertEquals(resource.getId(), response.getId()),
                () -> assertEquals(resource.getTitle(), response.getTitle())
        );
    }

}
