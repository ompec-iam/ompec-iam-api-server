package ru.ompec.iam.mapper;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.ompec.iam.domain.Department;
import ru.ompec.iam.payload.department.CreateDepartmentRequest;
import ru.ompec.iam.payload.department.DepartmentResponse;


import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class DepartmentMapperTest {

    private final DepartmentMapper mapper;

    @Autowired
    public DepartmentMapperTest(DepartmentMapper mapper) {
        this.mapper = mapper;
    }

    @Test
    public void contextLoad() {
        assertThat(mapper).isNotNull();
    }

    @Test
    public void testMapCreateDepartmentRequestToDepartment() {
        CreateDepartmentRequest request = new CreateDepartmentRequest("Title");

        Department department = mapper.createDepartmentRequestToDepartment(request);

        assertAll(
                () -> assertEquals(request.getTitle(), department.getTitle()),
                () -> assertThat(department.getEmployees()).isEmpty(),
                () -> assertThat(department.getId()).isNull()
        );
    }

    @Test
    public void testMapDepartmentToResponse() {
        Department department = Department.builder()
                                          .id(UUID.randomUUID())
                                          .title("Info")
                                          .build();

        DepartmentResponse response = mapper.toResponse(department);

        assertAll(
                () -> assertEquals(department.getId(), response.getId()),
                () -> assertEquals(department.getTitle(), response.getTitle())
        );
    }


}
