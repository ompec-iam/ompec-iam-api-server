package ru.ompec.iam.mapper;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.ompec.iam.domain.*;
import ru.ompec.iam.payload.permission.CreatePermissionRequest;
import ru.ompec.iam.payload.permission.PermissionResponse;
import ru.ompec.iam.repository.EmployeeRepository;
import ru.ompec.iam.repository.PermissionStatusRepository;
import ru.ompec.iam.repository.ResourceRepository;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class PermissionMapperTest {

    @MockBean
    private ResourceRepository resourceRepository;

    @MockBean
    private PermissionStatusRepository permissionStatusRepository;

    @MockBean
    private EmployeeRepository employeeRepository;

    @Autowired
    private PermissionMapper permissionMapper;

    @Test
    public void contextLoad() {
        assertThat(resourceRepository).isNotNull();
        assertThat(permissionStatusRepository).isNotNull();
        assertThat(employeeRepository).isNotNull();
        assertThat(permissionMapper).isNotNull();
    }

    @Test
    public void testMapToEntity() {
        PermissionStatus status = new PermissionStatus();
        status.setStatus("Active");
        status.setId(UUID.randomUUID());

        Resource resource = new Resource();
        resource.setTitle("Moodle");
        resource.setId(UUID.randomUUID());

        String login = "kirill_anatoly";
        String password = "mRT.8uKo_";

        Employee employee = Employee.builder()
                                    .id(UUID.randomUUID())
                                    .firstName("Анна")
                                    .lastName("Кулакова")
                                    .patronymic("Ивановна")
                                    .position("Бухгалтер")
                                    .phoneNumber("88008008800")
                                    .build();

        CreatePermissionRequest request = new CreatePermissionRequest(
                status.getId(),
                resource.getId(),
                employee.getId(),
                login,
                password
        );

        when(resourceRepository.findByIdFull(any())).thenReturn(Optional.of(resource));
        when(permissionStatusRepository.findByIdFull(any())).thenReturn(Optional.of(status));
        when(employeeRepository.findByIdFull(any())).thenReturn(Optional.of(employee));

        Permission permission = permissionMapper.toPermission(request);

        assertAll(
                () -> assertNull(permission.getId()),
                () -> assertEquals(resource.getId(), permission.getResource().getId()),
                () -> assertEquals(resource.getTitle(), permission.getResource().getTitle()),
                () -> assertEquals(status.getId(), permission.getStatus().getId()),
                () -> assertEquals(status.getStatus(), permission.getStatus().getStatus()),
                () -> assertEquals(employee, permission.getEmployee()),
                () -> assertNull(permission.getCredential().getId()),
                () -> assertEquals(login, permission.getCredential().getLogin()),
                () -> assertEquals(password, permission.getCredential().getPassword())
        );
    }

    @Test
    public void testMapPermissionToPesponse() {
        PermissionStatus status = new PermissionStatus();
        status.setStatus("Active");
        status.setId(UUID.randomUUID());

        Resource resource = new Resource();
        resource.setTitle("Moodle");
        resource.setId(UUID.randomUUID());

        String login = "kirill_anatoly";
        String password = "mRT.8uKo_";

        Credential credential = new Credential();
        credential.setId(UUID.randomUUID());
        credential.setLogin(login);
        credential.setPassword(password);

        Employee employee = Employee.builder()
                                    .id(UUID.randomUUID())
                                    .firstName("Анна")
                                    .lastName("Кулакова")
                                    .patronymic("Ивановна")
                                    .position("Бухгалтер")
                                    .phoneNumber("88008008800")
                                    .build();

        Permission permission = new Permission();
        permission.setStatus(status);
        permission.setResource(resource);
        permission.setEmployee(employee);
        permission.setCredential(credential);

        PermissionResponse response = permissionMapper.toResponse(permission);

        assertAll(
                () -> assertEquals(permission.getId(), response.getId()),
                () -> assertEquals(permission.getCredential().getId(), response.getCredential().getId()),
                () -> assertEquals(permission.getCredential().getLogin(), response.getCredential().getLogin()),
                () -> assertEquals(permission.getCredential().getPassword(), response.getCredential().getPassword()),
                () -> assertEquals(permission.getEmployee().getId(), response.getEmployeeId()),
                () -> assertEquals(permission.getResource().getId(), response.getResource().getId()),
                () -> assertEquals(permission.getResource().getTitle(), response.getResource().getTitle()),
                () -> assertEquals(permission.getStatus().getId(), response.getStatus().getId()),
                () -> assertEquals(permission.getStatus().getStatus(), response.getStatus().getStatus())
        );
    }



}
