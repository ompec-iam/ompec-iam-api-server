package ru.ompec.iam.mapper;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.ompec.iam.domain.PermissionStatus;
import ru.ompec.iam.payload.permissionstatus.CreatePermissionStatusRequest;
import ru.ompec.iam.payload.permissionstatus.PermissionStatusResponse;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class PermissionStatusMapperTest {

    @Autowired
    PermissionStatusMapper mapper;

    @Test
    public void contextLoad() {
        assertThat(mapper).isNotNull();
    }

    @Test
    public void testMapToEntity() {
        CreatePermissionStatusRequest request = new CreatePermissionStatusRequest(
                "Active",
                "#aaa"
        );

        PermissionStatus status = mapper.toEntity(request);

        assertAll(
                () -> assertEquals(request.getStatus(), status.getStatus()),
                () -> assertEquals(request.getColor(), status.getColor()),
                () -> assertThat(status.getId()).isNull()
        );
    }

    @Test
    public void testMapStatusToResponse() {
        PermissionStatus status = PermissionStatus.builder()
                                                  .id(UUID.randomUUID())
                                                  .status("Active")
                                                  .color("#aaa")
                                                  .build();

        PermissionStatusResponse response = mapper.toResponse(status);

        assertAll(
                () -> assertEquals(status.getId(), response.getId()),
                () -> assertEquals(status.getColor(), response.getColor()),
                () -> assertEquals(status.getStatus(), response.getStatus())
        );
    }

}
