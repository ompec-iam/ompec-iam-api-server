package ru.ompec.iam.mapper;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.ompec.iam.domain.Credential;
import ru.ompec.iam.payload.credential.CredentialResponse;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Sql(value = {"/create-sql-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/create-sql-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@ActiveProfiles("test")
public class CredentialMapperTest {

    private final CredentialMapper credentialMapper;

    @Autowired
    public CredentialMapperTest(CredentialMapper credentialMapper) {
        this.credentialMapper = credentialMapper;
    }

    @Test
    public void contextLoad() {
        assertThat(credentialMapper).isNotNull();
    }

    @WithUserDetails(value = "anya")
    @Test
    public void testMapCredentialToResponse() {
        Credential credential = Credential.builder()
                                          .id(UUID.randomUUID())
                                          .login("anita_shlyapa")
                                          .password("mLSD.21kMuKo_")
                                          .build();

        CredentialResponse response = credentialMapper.toResponse(credential);

        assertAll(
                () -> assertEquals(credential.getId(), response.getId()),
                () -> assertEquals(credential.getLogin(), response.getLogin()),
                () -> assertEquals(credential.getPassword(), response.getPassword())
        );
    }

}
