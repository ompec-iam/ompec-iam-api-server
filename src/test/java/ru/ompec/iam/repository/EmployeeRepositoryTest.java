package ru.ompec.iam.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.ompec.iam.domain.Department;
import ru.ompec.iam.domain.Employee;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Sql(value = {"/empty-database.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/empty-database.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@ActiveProfiles("test")
public class EmployeeRepositoryTest {

    private final TestEntityManager entityManager;
    private final EmployeeRepository repository;

    @Autowired
    public EmployeeRepositoryTest(TestEntityManager entityManager,
                                  EmployeeRepository repository) {
        this.entityManager = entityManager;
        this.repository = repository;
    }

    @Test
    public void contextLoad() {
        assertThat(entityManager).isNotNull();
        assertThat(repository).isNotNull();
    }

    @Test
    public void should_find_no_employees_if_repository_is_empty() {
        Iterable<Employee> employees = repository.findAll();
        assertThat(employees).isEmpty();
    }

    @Test
    public void should_save_employee_with_department() {
        Employee employee = Employee.builder()
                                    .firstName("Анна")
                                    .lastName("Кулакова")
                                    .patronymic("Ивановна")
                                    .position("Бухгалтер")
                                    .phoneNumber("88008008800")
                                    .build();

        Department department = Department.builder()
                                          .title("Moodle")
                                          .build();
        assertThat(department.getId()).isNull();
        entityManager.persist(department);
        assertThat(department.getId()).isNotNull();
        employee.setDepartment(department);

        assertThat(employee.getId()).isNull();
        repository.save(employee);
        assertThat(employee.getId()).isNotNull();

        Optional<Employee> employeeOptional = repository.findById(employee.getId());

        if(employeeOptional.isPresent()) {
            Employee employee1 = employeeOptional.get();
            assertEquals(employee, employee1);
        } else {
            fail();
        }
    }

}
