package ru.ompec.iam.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.ompec.iam.domain.PermissionStatus;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Sql(value = {"/empty-database.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/empty-database.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@ActiveProfiles("test")
public class PermissionStatusRepositoryTest {

    private final TestEntityManager entityManager;
    private final PermissionStatusRepository repository;

    @Autowired
    public PermissionStatusRepositoryTest(TestEntityManager entityManager,
                                          PermissionStatusRepository repository) {
        this.entityManager = entityManager;
        this.repository = repository;
    }

    @Test
    public void contextLoad() {
        assertThat(entityManager).isNotNull();
        assertThat(repository).isNotNull();
    }

    @Test
    public void should_no_find_permission_status_if_repository_id_empty() {
        Iterable<PermissionStatus> statuses = repository.findAll();
        assertThat(statuses).isEmpty();
    }

    @Test
    public void should_save_permission_status() {
        PermissionStatus status = PermissionStatus.builder()
                                                  .status("Active")
                                                  .build();
        assertThat(status.getId()).isNull();

        repository.save(status);
        assertThat(status.getId()).isNotNull();
    }

}
