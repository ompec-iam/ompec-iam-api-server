package ru.ompec.iam.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.ompec.iam.domain.Department;
import ru.ompec.iam.dto.response.ExtendedDepartmentDto;


import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Sql(value = {"/empty-database.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/empty-database.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@ActiveProfiles("test")
public class DepartmentRepositoryTest {

    private final TestEntityManager entityManager;
    private final DepartmentRepository repository;

    @Autowired
    public DepartmentRepositoryTest(TestEntityManager entityManager, DepartmentRepository repository) {
        this.entityManager = entityManager;
        this.repository = repository;
    }

    @Test
    public void contextLoad() {
        assertThat(entityManager).isNotNull();
        assertThat(repository).isNotNull();
    }

    @Test
    public void should_find_no_departments_if_repository_id_empty() {
        Iterable<Department> departments = repository.findAll();
        assertThat(departments).isEmpty();
    }

    @Sql(value = "/department/init_department_dto_test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Test
    public void should_return_all_departments_detailed() {
        UUID id = UUID.fromString("7bad0611-15d0-4d63-9462-756ce083d10a");
        Set<ExtendedDepartmentDto> departments = repository.findAllDetailed();
        assertThat(departments).hasSize(3);
        ExtendedDepartmentDto dto = departments.stream().filter(d -> d.getId().equals(id)).findAny().get();
        assertAll(
                () -> assertEquals(id, dto.getId()),
                () -> assertEquals("Нефтехимическое отделение", dto.getTitle()),
                () -> assertEquals(6, dto.getCount())
        );
    }

    @Sql(value = "/department/init_department_dto_test.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Test
    public void should_return_department_dto() {
        UUID id = UUID.fromString("7bad0611-15d0-4d63-9462-756ce083d10a");
        Optional<Department> departmentOptional = repository.findById(id);
        if(departmentOptional.isEmpty()) {
            fail();
        }
        Optional<ExtendedDepartmentDto> departmentDtoOptional = repository.findByIdDetailed(id);
        if(departmentDtoOptional.isEmpty()) {
            fail();
        }

        Department department = departmentOptional.get();
        ExtendedDepartmentDto departmentDto = departmentDtoOptional.get();

        Long count = departmentDto.getCount();

        assertAll(
                () -> assertEquals(department.getId(), departmentDto.getId()),
                () -> assertEquals(department.getTitle(), departmentDto.getTitle()),
                () -> assertEquals(6, count)
        );
    }

    @Test
    public void should_save_department() {
        Department department = Department.builder().title("Info").build();
        repository.save(department);

        assertThat(department.getId()).isNotNull();

        Department department1 = entityManager.find(Department.class, department.getId());

        assertAll(
                () -> assertEquals(department1.getId(), department.getId()),
                () -> assertEquals(department1.getTitle(), department.getTitle())
        );
    }

}
