package ru.ompec.iam.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.ompec.iam.domain.Resource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Sql(value = {"/empty-database.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/empty-database.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@ActiveProfiles("test")
public class ResourceRepositoryTest {

    private final TestEntityManager entityManager;
    private final ResourceRepository repository;

    @Autowired
    public ResourceRepositoryTest(TestEntityManager entityManager, ResourceRepository repository) {
        this.entityManager = entityManager;
        this.repository = repository;
    }

    @Test
    public void contextLoad() {
        assertThat(entityManager).isNotNull();
        assertThat(repository).isNotNull();
    }

    @Test
    public void should_find_no_resources_if_repository_id_empty() {
        Iterable<Resource> resources = repository.findAll();
        assertThat(resources).isEmpty();
    }

    @Test
    public void should_save_resource() {
        Resource resource = Resource.builder().title("Moodle").build();
        repository.save(resource);

        assertThat(resource.getId()).isNotNull();

        Resource resource1 = entityManager.find(Resource.class, resource.getId());

        assertAll(
                () -> assertEquals(resource1.getId(), resource.getId()),
                () -> assertEquals(resource1.getTitle(), resource.getTitle())
        );
    }

}
