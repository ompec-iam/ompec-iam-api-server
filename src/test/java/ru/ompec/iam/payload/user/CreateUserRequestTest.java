package ru.ompec.iam.payload.user;

import org.junit.jupiter.api.Test;
import ru.ompec.iam.payload.permission.CreatePermissionRequest;
import ru.ompec.iam.util.BaseValidationTest;

import javax.validation.ConstraintViolation;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class CreateUserRequestTest extends BaseValidationTest {

    @Test
    public void testWhenLoginIsBlank_thenFails() {
        CreateUserRequest request = new CreateUserRequest(
                "",
                "Mrt?_8iididiF]",
                UUID.randomUUID(),
                true
        );

        Set<ConstraintViolation<CreateUserRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Укажите логин");
    }

    @Test
    public void testWhenLoginContainsSpace_thenFails() {
        CreateUserRequest request = new CreateUserRequest(
                "ani_sh ark",
                "Mrt?_8iididiF]",
                UUID.randomUUID(),
                true
        );

        Set<ConstraintViolation<CreateUserRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Логин не может содержать пробелы");
    }

    @Test
    public void testWhenLoginLengthLessThan3Characters_thenFails() {
        CreateUserRequest request = new CreateUserRequest(
                "an",
                "Mrt?_8iididiF]",
                UUID.randomUUID(),
                true
        );

        Set<ConstraintViolation<CreateUserRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Длина логина должна быть между 3 и 20 символами");
    }

    @Test
    public void testWhenLoginLengthGreaterThan20Characters_thenFails() {
        CreateUserRequest request = new CreateUserRequest(
                "ani_sharkaaaaaaaaaaaaaaaaaaaaa",
                "Mrt?_8iididiF]",
                UUID.randomUUID(),
                true
        );

        Set<ConstraintViolation<CreateUserRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Длина логина должна быть между 3 и 20 символами");
    }

    @Test
    public void testWhenPasswordIsBlank_thenFails() {
        CreateUserRequest request = new CreateUserRequest(
                "ani_shark",
                "",
                UUID.randomUUID(),
                true
        );

        Set<ConstraintViolation<CreateUserRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Укажите пароль");
    }

    @Test
    public void testWhenPasswordContainsSpace_thenFails() {
        CreateUserRequest request = new CreateUserRequest(
                "ani_shark",
                "Mrt?_8 iididiF]",
                UUID.randomUUID(),
                true
        );

        Set<ConstraintViolation<CreateUserRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Пробелы в пароле не допустимы");
    }

    @Test
    public void testWhenPasswordLengthLessThan8Characters_thenFails() {
        CreateUserRequest request = new CreateUserRequest(
                "ani_shark",
                "Mrt?_8]",
                UUID.randomUUID(),
                true
        );

        Set<ConstraintViolation<CreateUserRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Длина пароля может находиться в пределах 8 - 40 символов");
    }

    @Test
    public void testWhenPasswordLengthGreaterThan40Characters_thenFails() {
        CreateUserRequest request = new CreateUserRequest(
                "ani_shark",
                "Mrt?_8]sddsdsdsdsdsdssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss",
                UUID.randomUUID(),
                true
        );

        Set<ConstraintViolation<CreateUserRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Длина пароля может находиться в пределах 8 - 40 символов");
    }

    @Test
    public void testWhenRoleIdIsNull_thenFails() {
        CreateUserRequest request = new CreateUserRequest(
                "ani_shark",
                "Mrt?_8iididiF]",
                null,
                true
        );

        Set<ConstraintViolation<CreateUserRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Укажите id роли");
    }

}
