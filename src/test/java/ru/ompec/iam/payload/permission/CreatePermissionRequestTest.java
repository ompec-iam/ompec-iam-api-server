package ru.ompec.iam.payload.permission;

import org.junit.jupiter.api.Test;
import ru.ompec.iam.util.BaseValidationTest;

import javax.validation.ConstraintViolation;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class CreatePermissionRequestTest extends BaseValidationTest {

    @Test
    public void testWhenPermissionStatusIdIsNull_thenFails() {
        CreatePermissionRequest request = new CreatePermissionRequest(
                null,
                UUID.randomUUID(),
                UUID.randomUUID(),
                "kirill_anatoly",
                "mRT.8uKo_"
        );

        Set<ConstraintViolation<CreatePermissionRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Отсутствует идентификатор статуса разрешения");
    }

    @Test
    public void testWhenResourceIdIsNull_thenFails() {
        CreatePermissionRequest request = new CreatePermissionRequest(
                UUID.randomUUID(),
                null,
                UUID.randomUUID(),
                "kirill_anatoly",
                "mRT.8uKo_"
        );

        Set<ConstraintViolation<CreatePermissionRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Отсутствует идентификатор ресурса");
    }

    @Test
    public void testWhenEmployeeIdIsNull_thenFails() {
        CreatePermissionRequest request = new CreatePermissionRequest(
                UUID.randomUUID(),
                UUID.randomUUID(),
                null,
                "kirill_anatoly",
                "mRT.8uKo_"
        );

        Set<ConstraintViolation<CreatePermissionRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Отсутствует идентификатор работника");
    }

    @Test
    public void testWhenLoginIsNull_thenFails() {
        CreatePermissionRequest request = new CreatePermissionRequest(
                UUID.randomUUID(),
                UUID.randomUUID(),
                UUID.randomUUID(),
                null,
                "mRT.8uKo_"
        );

        Set<ConstraintViolation<CreatePermissionRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Отсутствует логин");
    }

    @Test
    public void testWhenLoginIsEmpty_thenFails() {
        CreatePermissionRequest request = new CreatePermissionRequest(
                UUID.randomUUID(),
                UUID.randomUUID(),
                UUID.randomUUID(),
                "",
                "mRT.8uKo_"
        );

        Set<ConstraintViolation<CreatePermissionRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Отсутствует логин");
    }

    @Test
    public void testWhenLoginIsBlank_thenFails() {
        CreatePermissionRequest request = new CreatePermissionRequest(
                UUID.randomUUID(),
                UUID.randomUUID(),
                UUID.randomUUID(),
                " ",
                "mRT.8uKo_"
        );

        Set<ConstraintViolation<CreatePermissionRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Отсутствует логин");
    }

    @Test
    public void testWhenLoginContainsSpace_thenFails() {
        CreatePermissionRequest request = new CreatePermissionRequest(
                UUID.randomUUID(),
                UUID.randomUUID(),
                UUID.randomUUID(),
                "kirill _anatoly",
                "mRT.8uKo_"
        );

        Set<ConstraintViolation<CreatePermissionRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Пробелы в логине недопустимы");
    }

    @Test
    public void testWhenLoginLengthLessThan5Characters_thenFails() {
        CreatePermissionRequest request = new CreatePermissionRequest(
                UUID.randomUUID(),
                UUID.randomUUID(),
                UUID.randomUUID(),
                "anya",
                "mRT.8uKo_"
        );

        Set<ConstraintViolation<CreatePermissionRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Длина логина может находиться в пределах 5 - 50 символов");
    }

    @Test
    public void testWhenLoginLengthGreaterThan50Characters_thenFails() {
        CreatePermissionRequest request = new CreatePermissionRequest(
                UUID.randomUUID(),
                UUID.randomUUID(),
                UUID.randomUUID(),
                "sashasashasashasashasashasashasashasashasashasasha5",
                "mRT.8uKo_"
        );

        Set<ConstraintViolation<CreatePermissionRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Длина логина может находиться в пределах 5 - 50 символов");
    }

    @Test
    public void testWhenLoginContainsNonLatinCharacters_thenFails() {
        CreatePermissionRequest request = new CreatePermissionRequest(
                UUID.randomUUID(),
                UUID.randomUUID(),
                UUID.randomUUID(),
                "sasha_kirд",
                "mRT.8uKo_"
        );

        Set<ConstraintViolation<CreatePermissionRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Логин может состоять из латинских букв, цифр и символа '_'");
    }

    @Test
    public void testWhenPasswordIsNull_thenFails() {
        CreatePermissionRequest request = new CreatePermissionRequest(
                UUID.randomUUID(),
                UUID.randomUUID(),
                UUID.randomUUID(),
                "kirill_anatoly",
                null
        );

        Set<ConstraintViolation<CreatePermissionRequest>> violations = validator.validate(request);
        assertThat(violations).isEmpty();
    }

    @Test
    public void testWhenPasswordIsEmpty_thenFails() {
        CreatePermissionRequest request = new CreatePermissionRequest(
                UUID.randomUUID(),
                UUID.randomUUID(),
                UUID.randomUUID(),
                "kirill_anatoly",
                ""
        );

        Set<ConstraintViolation<CreatePermissionRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
    }

    @Test
    public void testWhenPasswordIsBlank_thenFails() {
        CreatePermissionRequest request = new CreatePermissionRequest(
                UUID.randomUUID(),
                UUID.randomUUID(),
                UUID.randomUUID(),
                "kirill_anatoly",
                " "
        );

        Set<ConstraintViolation<CreatePermissionRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
    }

    @Test
    public void testWhenPasswordContainsSpace_thenFails() {
        CreatePermissionRequest request = new CreatePermissionRequest(
                UUID.randomUUID(),
                UUID.randomUUID(),
                UUID.randomUUID(),
                "kirill_anatoly",
                "mRT.8u Ko_"
        );

        Set<ConstraintViolation<CreatePermissionRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Пробелы в пароле не допустимы");
    }

    @Test
    public void testWhenPasswordLengthLessThan8_thenFails() {
        CreatePermissionRequest request = new CreatePermissionRequest(
                UUID.randomUUID(),
                UUID.randomUUID(),
                UUID.randomUUID(),
                "kirill_anatoly",
                "mRT.mRT.8umRT.8umRT.8umRT.8umRT.8umRT.8umRT.8u"
        );

        Set<ConstraintViolation<CreatePermissionRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Длина пароля может находиться в пределах 8 - 40 символов");
    }

    @Test
    public void testWhenPasswordNotContainsRequiredCharacter_thenFails() {
        CreatePermissionRequest request = new CreatePermissionRequest(
                UUID.randomUUID(),
                UUID.randomUUID(),
                UUID.randomUUID(),
                "kirill_anatoly",
                "Mf6yTrrhfgdte$"
        );

        Set<ConstraintViolation<CreatePermissionRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Пароль должен содержать как минимум прописные и строчные буквы и как минимум один обязательный символ: '.', '/', '*', '[', ']', '_', '@', '?'");
    }

    @Test
    public void testWhenPasswordNotContainsUpperCaseLetter_thenFails() {
        CreatePermissionRequest request = new CreatePermissionRequest(
                UUID.randomUUID(),
                UUID.randomUUID(),
                UUID.randomUUID(),
                "kirill_anatoly",
                "mrt8uiilfkfofof_"
        );

        Set<ConstraintViolation<CreatePermissionRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Пароль должен содержать как минимум прописные и строчные буквы и как минимум один обязательный символ: '.', '/', '*', '[', ']', '_', '@', '?'");
    }

    @Test
    public void testWhenPasswordNotContainsLowerCaseLetter_thenFails() {
        CreatePermissionRequest request = new CreatePermissionRequest(
                UUID.randomUUID(),
                UUID.randomUUID(),
                UUID.randomUUID(),
                "kirill_anatoly",
                "MRT8UIILFFOFOF_"
        );

        Set<ConstraintViolation<CreatePermissionRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Пароль должен содержать как минимум прописные и строчные буквы и как минимум один обязательный символ: '.', '/', '*', '[', ']', '_', '@', '?'");
    }

}
