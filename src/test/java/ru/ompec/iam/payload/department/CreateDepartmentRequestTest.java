package ru.ompec.iam.payload.department;

import org.junit.jupiter.api.Test;
import ru.ompec.iam.util.BaseValidationTest;

import javax.validation.ConstraintViolation;

import java.util.Set;
import static org.assertj.core.api.Assertions.assertThat;

public class CreateDepartmentRequestTest extends BaseValidationTest {

    @Test
    public void whenTitleIsBlank_thenFails() {
        CreateDepartmentRequest request = new CreateDepartmentRequest("");
        assertThat(request.getTitle()).isEmpty();

        Set<ConstraintViolation<CreateDepartmentRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Отсутствует название отдела");
    }

    @Test
    public void whenTitle_thenOk() {
        CreateDepartmentRequest request = new CreateDepartmentRequest("Нефтехимическое отделение");
        assertThat(request.getTitle()).isNotNull();
        Set<ConstraintViolation<CreateDepartmentRequest>> violations = validator.validate(request);
        assertThat(violations).isEmpty();
    }

}
