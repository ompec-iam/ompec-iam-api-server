package ru.ompec.iam.payload.department;

import org.junit.jupiter.api.Test;
import ru.ompec.iam.util.BaseValidationTest;

import javax.validation.ConstraintViolation;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class UpdateDepartmentRequestTest extends BaseValidationTest {

    @Test
    public void whenDepartmentIdIsBlank_thenFails() {
        UpdateDepartmentRequest request = new UpdateDepartmentRequest(
                null,
                "INFO"
        );

        Set<ConstraintViolation<UpdateDepartmentRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Отсутствует идентификатор отдела");
    }

    @Test
    public void whenTitleIsBlank_thenFails() {
        UpdateDepartmentRequest request = new UpdateDepartmentRequest(
                UUID.randomUUID(),
                ""
        );

        Set<ConstraintViolation<UpdateDepartmentRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Отсутствует название отдела");
    }

}
