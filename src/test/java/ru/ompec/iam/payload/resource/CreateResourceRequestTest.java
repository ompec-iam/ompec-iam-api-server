package ru.ompec.iam.payload.resource;

import org.junit.jupiter.api.Test;
import ru.ompec.iam.payload.department.CreateDepartmentRequest;
import ru.ompec.iam.util.BaseValidationTest;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class CreateResourceRequestTest extends BaseValidationTest {

    @Test
    public void whenTitleIsBlank_thenFails() {
        CreateResourceRequest request = new CreateResourceRequest("");
        assertThat(request.getTitle()).isEmpty();

        Set<ConstraintViolation<CreateResourceRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Отсутствует название ресурса");
    }

    @Test
    public void whenTitle_thenOk() {
        CreateResourceRequest request = new CreateResourceRequest("Moodle");
        assertThat(request.getTitle()).isNotNull();
        Set<ConstraintViolation<CreateResourceRequest>> violations = validator.validate(request);
        assertThat(violations).isEmpty();
    }
}
