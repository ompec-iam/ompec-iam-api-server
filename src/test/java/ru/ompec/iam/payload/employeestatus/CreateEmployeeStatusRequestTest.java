package ru.ompec.iam.payload.employeestatus;

import org.junit.jupiter.api.Test;
import ru.ompec.iam.util.BaseValidationTest;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class CreateEmployeeStatusRequestTest extends BaseValidationTest {

    @Test
    public void testWhenStatusIsEmpty_thenFails() {
        CreateEmployeeStatusRequest request = new CreateEmployeeStatusRequest(
                "",
                "#aaa"
        );

        Set<ConstraintViolation<CreateEmployeeStatusRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Отсутствует название статуса");
    }

    @Test
    public void testWhenStatusGreaterThan40Characters_thenFails() {
        CreateEmployeeStatusRequest request = new CreateEmployeeStatusRequest(
                "activeactiveactiveactiveactiveactiveactiveactiveactiveactiveactiveactiveactive",
                "#aaa"
        );

        Set<ConstraintViolation<CreateEmployeeStatusRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Название статуса не должно быть длиннее 40 символов");
    }

    @Test
    public void testWhenColorIsEmpty_thenFails() {
        CreateEmployeeStatusRequest request = new CreateEmployeeStatusRequest(
                "active",
                ""
        );

        Set<ConstraintViolation<CreateEmployeeStatusRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Отсутствует цвет метки");
    }


}
