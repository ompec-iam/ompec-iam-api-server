package ru.ompec.iam.payload.employee;

import org.junit.jupiter.api.Test;
import ru.ompec.iam.util.BaseValidationTest;

import javax.validation.ConstraintViolation;
import java.util.Set;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class CreateEmployeeRequestTest extends BaseValidationTest {

    @Test
    public void testWhenFirstNameIsNull_thenFails() {
        CreateEmployeeRequest request = new CreateEmployeeRequest(
                null,
                null,
                null,
                null,
                null,
                null,
                null
        );

        Set<ConstraintViolation<CreateEmployeeRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Отсутствует имя");
    }

    @Test
    public void testWhenLastNameIsNull_thenFails() {
        CreateEmployeeRequest request = new CreateEmployeeRequest(
                "Анатолий",
                null,
                null,
                null,
                null,
                null,
                null
        );

        Set<ConstraintViolation<CreateEmployeeRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Отсутствует фамилия");
    }

    @Test
    public void testWhenPositionIsNull_thenFails() {
        CreateEmployeeRequest request = new CreateEmployeeRequest(
                "Анатолий",
                "Упячев",
                null,
                null,
                null,
                null,
                null
        );

        Set<ConstraintViolation<CreateEmployeeRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Укажите должность");
    }

    @Test
    public void testWhenDepartmentIdIsNull_thenFails() {
        CreateEmployeeRequest request = new CreateEmployeeRequest(
                "Анатолий",
                "Упячев",
                null,
                "Бухгалтер",
                null,
                null,
                null
        );

        Set<ConstraintViolation<CreateEmployeeRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Отсутствует идентификатор отдела");
    }

    @Test
    public void testRequiredRequest() {
        CreateEmployeeRequest request = new CreateEmployeeRequest(
                "Анатолий",
                "Упячев",
                null,
                "Бухгалтер",
                null,
                UUID.randomUUID(),
                UUID.randomUUID()
        );

        Set<ConstraintViolation<CreateEmployeeRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(0);
    }

    @Test
    public void testWhenFirstNameGreaterThan50Characters_thenFails() {
        CreateEmployeeRequest request = new CreateEmployeeRequest(
                "АнатолийАнатолийАнатолийАнатолийАнатолийАнатолийАнатолийАнатолийАнатолий",
                "Упячев",
                null,
                "Бухгалтер",
                null,
                UUID.randomUUID(),
                UUID.randomUUID()
        );

        Set<ConstraintViolation<CreateEmployeeRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Имя не должно быть длиннее 50 символов");
    }

    @Test
    public void testWhenLastNameGreaterThan50Characters_thenFails() {
        CreateEmployeeRequest request = new CreateEmployeeRequest(
                "Анатолий",
                "УпячевУпячевУпячевУпячевУпячевУпячевУпячевУпячевУпячевУпячевУпячевУпячевУпячев",
                null,
                "Бухгалтер",
                null,
                UUID.randomUUID(),
                UUID.randomUUID()
        );

        Set<ConstraintViolation<CreateEmployeeRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Фамилия не должна быть длиннее 50 символов");
    }

    @Test
    public void testWhenPatronymicGreaterThan50Characters_thenFails() {
        CreateEmployeeRequest request = new CreateEmployeeRequest(
                "Анатолий",
                "Упячев",
                "АлександровичАлександровичАлександровичАлександровичАлександровичАлександровичАлександрович",
                "Бухгалтер",
                null,
                UUID.randomUUID(),
                UUID.randomUUID()
        );

        Set<ConstraintViolation<CreateEmployeeRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Отчество не должно быть длиннее 50 символов");
    }

    @Test
    public void testWhenPositionGreaterThan250Characters_thenFails() {
        CreateEmployeeRequest request = new CreateEmployeeRequest(
                "Анатолий",
                "Упячев",
                null,
                "БухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтерБухгалтер",
                null,
                UUID.randomUUID(),
                UUID.randomUUID()
        );

        Set<ConstraintViolation<CreateEmployeeRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Название должности должно быть короче 250 символов");
    }

    @Test
    public void whenFirstNameContainsDigits_thenFails() {
        CreateEmployeeRequest request = new CreateEmployeeRequest(
                "Анатолий3",
                "Упячев",
                null,
                "Бухгалтер",
                null,
                UUID.randomUUID(),
                UUID.randomUUID()
        );

        Set<ConstraintViolation<CreateEmployeeRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Имя должно состоять только из букв");
    }

    @Test
    public void whenLastNameContainsDigits_thenFails() {
        CreateEmployeeRequest request = new CreateEmployeeRequest(
                "Анатолий",
                "Упячев3",
                null,
                "Бухгалтер",
                null,
                UUID.randomUUID(),
                UUID.randomUUID()
        );

        Set<ConstraintViolation<CreateEmployeeRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Фамилия должна состоять только из букв");
    }

    @Test
    public void whenPatronymicContainsDigits_thenFails() {
        CreateEmployeeRequest request = new CreateEmployeeRequest(
                "Анатолий",
                "Упячев",
                "Александрович3",
                "Бухгалтер",
                null,
                UUID.randomUUID(),
                UUID.randomUUID()
        );

        Set<ConstraintViolation<CreateEmployeeRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Отчество должно состоять только из букв");
    }

}
