package ru.ompec.iam.payload.permissionstatus;

import org.junit.jupiter.api.Test;
import ru.ompec.iam.payload.employee.CreateEmployeeRequest;
import ru.ompec.iam.util.BaseValidationTest;

import javax.validation.ConstraintViolation;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class CreatePermissionStatusRequestTest extends BaseValidationTest {

    @Test
    public void testWhenStatusIsNull_thenFails() {
        CreatePermissionStatusRequest request = new CreatePermissionStatusRequest(
                null,
                "#aaa"
        );

        Set<ConstraintViolation<CreatePermissionStatusRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Отсутствует название статуса");
    }

    @Test
    public void testWhenStatusIsEmpty_thenFails() {
        CreatePermissionStatusRequest request = new CreatePermissionStatusRequest(
                "",
                "#aaa"
        );

        Set<ConstraintViolation<CreatePermissionStatusRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Отсутствует название статуса");
    }

    @Test
    public void testWhenStatusGreaterThan40Characters_thenFails() {
        CreatePermissionStatusRequest request = new CreatePermissionStatusRequest(
                "activeactiveactiveactiveactiveactiveactiveactiveactiveactive",
                "#aaa"
        );

        Set<ConstraintViolation<CreatePermissionStatusRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Название статуса не должно быть длиннее 40 символов");
    }

    @Test
    public void testWhenColorIsEmpty_thenFails() {
        CreatePermissionStatusRequest request = new CreatePermissionStatusRequest(
                "active",
                ""
        );

        Set<ConstraintViolation<CreatePermissionStatusRequest>> violations = validator.validate(request);
        assertThat(violations).hasSize(1);
        assertThat(constraintViolationsToStringSet(violations)).contains("Отсутствует цвет метки");
    }

}
