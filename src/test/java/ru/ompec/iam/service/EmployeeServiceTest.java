package ru.ompec.iam.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.ompec.iam.mapper.EmployeeMapper;
import ru.ompec.iam.repository.DepartmentRepository;
import ru.ompec.iam.repository.EmployeeRepository;
import ru.ompec.iam.repository.EmployeeStatusRepository;


import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class EmployeeServiceTest {

    @Mock
    private EmployeeRepository employeeRepository;

    @Mock
    private DepartmentRepository departmentRepository;

    @Mock
    private EmployeeStatusRepository statusRepository;

    @Mock
    private EmployeeMapper employeeMapper;

    @Mock
    private FileStorageServiceImpl fileStorageService;

    private EmployeeService employeeService;

    @BeforeEach
    public void init() {
        employeeService = new EmployeeService(employeeRepository, employeeMapper, fileStorageService, departmentRepository, statusRepository);
    }

    @Test
    public void contextLoad() {
        assertThat(employeeRepository).isNotNull();
        assertThat(employeeMapper).isNotNull();
        assertThat(employeeService).isNotNull();
    }

}
