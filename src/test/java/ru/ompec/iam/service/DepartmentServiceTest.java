package ru.ompec.iam.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.ompec.iam.domain.Department;
import ru.ompec.iam.mapper.DepartmentMapper;
import ru.ompec.iam.payload.department.CreateDepartmentRequest;
import ru.ompec.iam.repository.DepartmentRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class DepartmentServiceTest {

    @Mock
    private DepartmentRepository departmentRepository;

    @Mock
    private DepartmentMapper mapper;

    private DepartmentService departmentService;

    @BeforeEach
    public void init() {
        departmentService = new DepartmentService(mapper, departmentRepository);
    }

    @Test
    public void contextLoad() {
        assertThat(departmentRepository).isNotNull();
        assertThat(mapper).isNotNull();
        assertThat(departmentService).isNotNull();
    }

    @Test
    @WithMockUser(username = "anya", roles = { "ADMIN" })
    public void shouldReturnDepartmentIfUserIsAdmin() throws Exception {
        String departmentTitle = "Info";
        CreateDepartmentRequest request = new CreateDepartmentRequest(departmentTitle);
        Department department = Department.builder().title(departmentTitle).build();

        when(departmentRepository.existsByTitle(eq(departmentTitle))).thenReturn(false);
        when(mapper.createDepartmentRequestToDepartment(eq(request))).thenReturn(department);
        when(departmentRepository.save(eq(department))).thenReturn(department);

        Department department1 = departmentService.createDepartment(request);

        assertThat(department.getTitle()).isEqualTo(departmentTitle);
    }

    /*@Test
    @WithMockUser(username = "Maxim")
    public void shouldThrowAccessDeniedExceptionWhenCreateDepartment() throws Exception {
        CreateDepartmentRequest request = new CreateDepartmentRequest("Info");

        AccessDeniedException exception = assertThrows(AccessDeniedException.class, () -> {
            departmentService.createDepartment(request);
        });
    }*/ // - not working

}
