package ru.ompec.iam.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.ompec.iam.exception.DuplicateResourceException;
import ru.ompec.iam.mapper.PermissionStatusMapper;
import ru.ompec.iam.payload.permissionstatus.CreatePermissionStatusRequest;
import ru.ompec.iam.repository.PermissionStatusRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Sql(value = {"/create-sql-before.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/create-sql-after.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
@ActiveProfiles("test")
public class PermissionStatusServiceTest {

    @Mock
    private PermissionStatusRepository repository;

    @Mock
    private PermissionStatusMapper mapper;

    private PermissionStatusService service;

    @BeforeEach
    public void init() {
        service = new PermissionStatusService(repository, mapper);
    }

    @Test
    public void contextLoad() {
        assertThat(repository).isNotNull();
        assertThat(mapper).isNotNull();
        assertThat(service).isNotNull();
    }

    /*@WithMockUser(value = "maxim")
    @Test
    public void should_throw_access_denied_when_create_permission_status_by_user() {
        CreatePermissionStatusRequest request = new CreatePermissionStatusRequest("Active");

        AccessDeniedException exception = assertThrows(AccessDeniedException.class, () -> {
            service.createPermissionStatus(request);
        });
        assertThat(exception).isNotNull();
    }*/ // - not working

    @WithUserDetails(value = "anya")
    @Test
    public void should_throw_duplicate_resource_exception_when_create_permission_status() {
        CreatePermissionStatusRequest request = new CreatePermissionStatusRequest("Active", "#aaa");

        when(repository.existsByStatus(any())).thenReturn(true);

        DuplicateResourceException exception = assertThrows(DuplicateResourceException.class, () -> {
            service.createPermissionStatus(request);
        });
        assertThat(exception.getMessage()).contains(request.getStatus());
    }

}
