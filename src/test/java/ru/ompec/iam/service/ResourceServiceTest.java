package ru.ompec.iam.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import ru.ompec.iam.domain.Resource;
import ru.ompec.iam.mapper.ResourceMapper;
import ru.ompec.iam.payload.resource.CreateResourceRequest;
import ru.ompec.iam.repository.ResourceRepository;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class ResourceServiceTest {

    @Mock
    private ResourceRepository resourceRepository;

    @Mock
    private ResourceMapper mapper;

    private ResourceService resourceService;

    @BeforeEach
    public void init() {
        resourceService = new ResourceService(mapper, resourceRepository);
    }

    @Test
    public void contextLoad() {
        assertThat(resourceRepository).isNotNull();
        assertThat(mapper).isNotNull();
        assertThat(resourceService).isNotNull();
    }

    @Test
    @WithMockUser(username = "anya", roles = { "ADMIN" })
    public void shouldReturnResourceIfUserIdAdmin() throws Exception {
        String resourceTitle = "Moodle";
        CreateResourceRequest request = new CreateResourceRequest(resourceTitle);
        Resource resource = Resource.builder().id(UUID.randomUUID()).title(resourceTitle).build();

        when(resourceRepository.existsByTitle(eq(resourceTitle))).thenReturn(false);
        when(mapper.createResourceRequestToResource(eq(request))).thenReturn(resource);
        when(resourceRepository.save(eq(resource))).thenReturn(resource);

        Resource resource1 = resourceService.createResource(request);

        assertThat(resource1.getTitle()).isEqualTo(resourceTitle);
    }

    /*@Test
    @WithMockUser("Maxim")
    public void shouldThrowAccessDeniedExceptionWhenCreateResourceIfUserIsNotAdmin() throws Exception {
        String resourceTitle = "Moodle";
        CreateResourceRequest request = new CreateResourceRequest(resourceTitle);
        Resource resource = Resource.builder().id(UUID.randomUUID()).title(resourceTitle).build();

        when(resourceRepository.existsByTitle(eq(resourceTitle))).thenReturn(false);
        when(mapper.createResourceRequestToResource(eq(request))).thenReturn(resource);
        when(resourceRepository.save(eq(resource))).thenReturn(resource);

        AccessDeniedException exception = assertThrows(AccessDeniedException.class, () -> {
            resourceService.createResource(request);
        });
    }*/ // - not working | or @InjectBeans not inject repository and mapper
}
