delete from user_role;
delete from rle;
delete from usr;
delete from department;
delete from credential;
delete from permission;
delete from permission_status;
delete from permission_credential;
delete from resource;
delete from employee;

insert into rle(id, name) values('4869ce38-d1b2-4fe1-a60a-27c4485d90c0', 'ROLE_ADMIN');
insert into rle(id, name) values('ea5899aa-ae8e-4ce5-8144-0cea6676e96f', 'ROLE_USER');

insert into usr(id, username, password_hash, is_active) values('ad303c4f-bde2-4e69-9018-bab9cfe0075e', 'anya', '$2a$10$/T3UuN2F3mNdRuM3b.8ZbuvtXxt9jrgTyblu/KtrFb8J5KqLWXi7i', true);
insert into user_role(user_id, role_id) values('ad303c4f-bde2-4e69-9018-bab9cfe0075e', '4869ce38-d1b2-4fe1-a60a-27c4485d90c0');

insert into usr(id, username, password_hash, is_active) values('67942671-bd0e-4647-8b78-37b9d416d08e', 'kirill', '$2a$10$/T3UuN2F3mNdRuM3b.8ZbuvtXxt9jrgTyblu/KtrFb8J5KqLWXi7i', true);
insert into user_role(user_id, role_id) values('67942671-bd0e-4647-8b78-37b9d416d08e', 'ea5899aa-ae8e-4ce5-8144-0cea6676e96f');
