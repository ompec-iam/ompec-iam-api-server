package ru.ompec.iam.mapper;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.ompec.iam.domain.User;
import ru.ompec.iam.payload.user.UserResponse;

import java.util.List;

@Mapper(
        componentModel = "spring",
        uses = {
                RoleMapper.class,
                TokenMapper.class
        },
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE,
        builder = @Builder(disableBuilder = true)
)
public interface UserMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "username", source = "username")
    @Mapping(target = "active", source = "active")
    @Mapping(target = "roles", source = "roles")
    @Mapping(target = "authTokens", source = "authTokens")
    UserResponse toResponse(User user);

    List<UserResponse> toResponses(List<User> users);

}
