package ru.ompec.iam.mapper;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.ompec.iam.domain.Resource;
import ru.ompec.iam.dto.response.ExtendedResourceDto;
import ru.ompec.iam.payload.resource.CreateResourceRequest;
import ru.ompec.iam.payload.resource.ExtendedResourceResponse;
import ru.ompec.iam.payload.resource.ResourceResponse;

import java.util.Set;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE,
        builder = @Builder(disableBuilder = true)
)
public interface ResourceMapper {

    Resource createResourceRequestToResource(CreateResourceRequest request);

    ResourceResponse toResponse(Resource resource);

    @Mapping(target = "id", expression = "java(resource.getId())")
    @Mapping(target = "title", expression = "java(resource.getTitle())")
    @Mapping(target = "permissionCount", expression = "java(resource.getCount())")
    ExtendedResourceResponse toDetailedResponse(ExtendedResourceDto resource);

    Set<ExtendedResourceResponse> toDetailedResponses(Set<ExtendedResourceDto> dtos);

}
