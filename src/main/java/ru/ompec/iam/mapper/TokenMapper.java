package ru.ompec.iam.mapper;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.ompec.iam.domain.Token;
import ru.ompec.iam.payload.token.TokenResponse;

import java.util.List;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE,
        builder = @Builder(disableBuilder = true)
)
public interface TokenMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "userId", source = "user.id")
    @Mapping(target = "ipAddress", source = "ipAddress")
    @Mapping(target = "deviceInfo", source = "deviceInfo")
    TokenResponse toResponse(Token token);

    List<TokenResponse> toResponses(List<Token> tokens);

}
