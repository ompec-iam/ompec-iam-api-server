package ru.ompec.iam.mapper;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.ompec.iam.domain.EmployeeStatus;
import ru.ompec.iam.dto.response.ExtendedEmployeeStatusDto;
import ru.ompec.iam.payload.employeestatus.CreateEmployeeStatusRequest;
import ru.ompec.iam.payload.employeestatus.EmployeeStatusResponse;
import ru.ompec.iam.payload.employeestatus.ExtendedEmployeeStatusResponse;

import java.util.Set;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE,
        builder = @Builder(disableBuilder = true)
)
public interface EmployeeStatusMapper {

    EmployeeStatus toEntity(CreateEmployeeStatusRequest request);
    EmployeeStatusResponse toResponse(EmployeeStatus status);

    @Mapping(target = "id", expression = "java(dto.getId())")
    @Mapping(target = "status", expression = "java(dto.getStatus())")
    @Mapping(target = "color", expression = "java(dto.getColor())")
    @Mapping(target = "employeeCount", expression = "java(dto.getCount())")
    ExtendedEmployeeStatusResponse toDetailedResponse(ExtendedEmployeeStatusDto dto);

    Set<ExtendedEmployeeStatusResponse> toDetailedResponses(Set<ExtendedEmployeeStatusDto> dtos);

}
