package ru.ompec.iam.mapper;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.security.core.GrantedAuthority;
import ru.ompec.iam.domain.Employee;
import ru.ompec.iam.payload.employee.CreateEmployeeRequest;
import ru.ompec.iam.payload.employee.EmployeeResponse;

@Mapper(
        componentModel = "spring",
        uses = {
                EmployeeMapperResolver.class,
                EmployeeStatusHelper.class,
                PermissionMapper.class
        },
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE,
        builder = @Builder(disableBuilder = true)
)
public interface EmployeeMapper {

    @Mapping(source = "departmentId", target = "department")
    @Mapping(source = "firstName", target = "firstName")
    @Mapping(source = "lastName", target = "lastName")
    @Mapping(source = "patronymic", target = "patronymic")
    @Mapping(source = "position", target = "position")
    @Mapping(source = "phoneNumber", target = "phoneNumber")
    @Mapping(source = "employeeStatusId", target = "status")
    Employee toEntity(CreateEmployeeRequest request);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "firstName", target = "firstName")
    @Mapping(source = "lastName", target = "lastName")
    @Mapping(source = "patronymic", target = "patronymic")
    @Mapping(source = "position", target = "position")
    @Mapping(source = "phoneNumber", target = "phoneNumber")
    @Mapping(source = "status", target = "status")
    @Mapping(source = "department.id", target = "department.id")
    @Mapping(source = "department.title", target = "department.title")
    EmployeeResponse toResponse(Employee employee);

}
