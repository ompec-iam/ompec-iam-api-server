package ru.ompec.iam.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ompec.iam.domain.Department;
import ru.ompec.iam.exception.ResourceNotFoundException;
import ru.ompec.iam.repository.DepartmentRepository;

import java.util.Optional;
import java.util.UUID;

@Component
public class EmployeeMapperResolver {

    private final DepartmentRepository departmentRepository;

    @Autowired
    public EmployeeMapperResolver(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    public Department map(UUID value) {
        Optional<Department> departmentOptional = departmentRepository.findById(value);
        if(departmentOptional.isEmpty()) {
            throw new ResourceNotFoundException("Department", "id", value);
        }

        return departmentOptional.get();
    }


}
