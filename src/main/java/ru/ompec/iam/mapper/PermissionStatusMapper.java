package ru.ompec.iam.mapper;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.ompec.iam.domain.PermissionStatus;
import ru.ompec.iam.dto.response.ExtendedPermissionStatusDto;
import ru.ompec.iam.payload.permissionstatus.CreatePermissionStatusRequest;
import ru.ompec.iam.payload.permissionstatus.ExtendedPermissionStatusResponse;
import ru.ompec.iam.payload.permissionstatus.PermissionStatusResponse;

import java.util.Set;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE,
        builder = @Builder(disableBuilder = true)
)
public interface PermissionStatusMapper {

    PermissionStatus toEntity(CreatePermissionStatusRequest request);

    PermissionStatusResponse toResponse(PermissionStatus permissionStatus);

    @Mapping(target = "id", expression = "java(permissionStatus.getId())")
    @Mapping(target = "status", expression = "java(permissionStatus.getStatus())")
    @Mapping(target = "color", expression = "java(permissionStatus.getColor())")
    @Mapping(target = "permissionCount", expression = "java(permissionStatus.getCount())")
    ExtendedPermissionStatusResponse toDetailedResponse(ExtendedPermissionStatusDto permissionStatus);

    Set<ExtendedPermissionStatusResponse> toDetailedResponses(Set<ExtendedPermissionStatusDto> dtos);

}
