package ru.ompec.iam.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ompec.iam.domain.EmployeeStatus;
import ru.ompec.iam.exception.ResourceNotFoundException;
import ru.ompec.iam.repository.EmployeeStatusRepository;

import java.util.UUID;

@Component
public class EmployeeStatusHelper {

    private final EmployeeStatusRepository repository;

    @Autowired
    public EmployeeStatusHelper(
        EmployeeStatusRepository repository
    ) {
        this.repository = repository;
    }

    public EmployeeStatus map(UUID id) {
        return repository.findByIdFull(id)
                         .orElseThrow(() -> new ResourceNotFoundException("EmployeeStatus", "id", id));
    }


}
