package ru.ompec.iam.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ompec.iam.domain.Employee;
import ru.ompec.iam.exception.ResourceNotFoundException;
import ru.ompec.iam.repository.EmployeeRepository;

import java.util.UUID;

@Component
public class EmployeeHelper {

    private final EmployeeRepository employeeRepository;

    @Autowired
    public EmployeeHelper(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public Employee map(UUID id) {
        Employee employee = employeeRepository.findByIdFull(id)
                                              .orElseThrow(() -> new ResourceNotFoundException("Employee", "id", id));
        return employee;
    }
}
