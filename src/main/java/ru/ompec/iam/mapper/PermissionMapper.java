package ru.ompec.iam.mapper;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.ompec.iam.domain.Permission;
import ru.ompec.iam.payload.permission.CreatePermissionRequest;
import ru.ompec.iam.payload.permission.PermissionResponse;

@Mapper(
        componentModel = "spring",
        uses = {
                PermissionStatusHelper.class,
                ResourceHelper.class,
                EmployeeHelper.class,
                PermissionStatusMapper.class,
                ResourceMapper.class,
                CredentialMapper.class
        },
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE,
        builder = @Builder(disableBuilder = true)
)
public interface PermissionMapper {

    @Mapping(target = "creationDate", expression = "java(java.time.LocalDateTime.now())")
    @Mapping(source = "permissionStatusId", target = "status")
    @Mapping(source = "resourceId", target = "resource")
    @Mapping(source = "employeeId", target = "employee")
    @Mapping(source = "login", target = "credential.login")
    @Mapping(source = "password", target = "credential.password")
    Permission toPermission(CreatePermissionRequest request);

    @Mapping(source = "employee.id", target = "employeeId")
    PermissionResponse toResponse(Permission permission);

}
