package ru.ompec.iam.mapper;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import ru.ompec.iam.domain.Credential;
import ru.ompec.iam.payload.credential.CredentialResponse;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE,
        builder = @Builder(disableBuilder = true)
)
public interface CredentialMapper {

    CredentialResponse toResponse(Credential credential);

}
