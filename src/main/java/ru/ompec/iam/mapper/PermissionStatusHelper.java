package ru.ompec.iam.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ompec.iam.domain.PermissionStatus;
import ru.ompec.iam.exception.ResourceNotFoundException;
import ru.ompec.iam.repository.PermissionStatusRepository;

import java.util.UUID;

@Component
public class PermissionStatusHelper {

    private final PermissionStatusRepository permissionStatusRepository;

    @Autowired
    public PermissionStatusHelper(PermissionStatusRepository permissionStatusRepository) {
        this.permissionStatusRepository = permissionStatusRepository;
    }

    public PermissionStatus map(UUID id) {
        PermissionStatus status = permissionStatusRepository.findByIdFull(id)
                                                            .orElseThrow(() -> new ResourceNotFoundException("PermissionStatus", "id", id));
        return status;
    }
}
