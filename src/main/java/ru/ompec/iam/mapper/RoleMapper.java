package ru.ompec.iam.mapper;

import org.mapstruct.Builder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.ompec.iam.domain.Role;
import ru.ompec.iam.payload.role.RoleResponse;

import java.util.List;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE,
        builder = @Builder(disableBuilder = true)
)
public interface RoleMapper {

    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    RoleResponse toResponse(Role role);

    List<RoleResponse> toResponses(List<Role> roles);
}
