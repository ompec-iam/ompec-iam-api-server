package ru.ompec.iam.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.ompec.iam.domain.Resource;
import ru.ompec.iam.exception.ResourceNotFoundException;
import ru.ompec.iam.repository.ResourceRepository;

import java.util.UUID;

@Component
public class ResourceHelper {

    private final ResourceRepository resourceRepository;

    @Autowired
    public ResourceHelper(ResourceRepository resourceRepository) {
        this.resourceRepository = resourceRepository;
    }

    public Resource map(UUID id) {
        Resource resource = resourceRepository.findByIdFull(id)
                                              .orElseThrow(() -> new ResourceNotFoundException("Resource", "id", id));
        return resource;
    }

}
