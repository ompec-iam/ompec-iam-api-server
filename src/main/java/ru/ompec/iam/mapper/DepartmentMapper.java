package ru.ompec.iam.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.ompec.iam.domain.Department;
import ru.ompec.iam.dto.response.ExtendedDepartmentDto;
import ru.ompec.iam.payload.department.CreateDepartmentRequest;
import ru.ompec.iam.payload.department.DepartmentResponse;
import ru.ompec.iam.payload.department.ExtendedDepartmentResponse;

import java.util.Set;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = org.mapstruct.ReportingPolicy.IGNORE
)
public interface DepartmentMapper {

    Department createDepartmentRequestToDepartment(CreateDepartmentRequest request);

    DepartmentResponse toResponse(Department department);

    @Mapping(target = "id", expression = "java(department.getId())")
    @Mapping(target = "title", expression = "java(department.getTitle())")
    @Mapping(target = "employeeCount", expression = "java(department.getCount())")
    ExtendedDepartmentResponse toDetailedResponse(ExtendedDepartmentDto department);

    Set<ExtendedDepartmentResponse> toDetailedResponses(Set<ExtendedDepartmentDto> dtos);

}
