package ru.ompec.iam.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import ru.ompec.iam.domain.EmployeeStatus;
import ru.ompec.iam.dto.response.ExtendedEmployeeStatusDto;
import ru.ompec.iam.exception.DuplicateResourceException;
import ru.ompec.iam.exception.ResourceNotFoundException;
import ru.ompec.iam.mapper.EmployeeStatusMapper;
import ru.ompec.iam.payload.employeestatus.CreateEmployeeStatusRequest;
import ru.ompec.iam.payload.employeestatus.DeleteEmployeeStatusRequest;
import ru.ompec.iam.payload.employeestatus.UpdateEmployeeStatusRequest;
import ru.ompec.iam.repository.EmployeeStatusRepository;

import java.util.Set;
import java.util.UUID;

@Service
@Transactional
public class EmployeeStatusService {

    private final EmployeeStatusMapper statusMapper;
    private final EmployeeStatusRepository repository;

    @Autowired
    public EmployeeStatusService(
            EmployeeStatusMapper statusMapper,
            EmployeeStatusRepository repository
    ) {
        this.statusMapper = statusMapper;
        this.repository = repository;
    }

    @Secured("ROLE_ADMIN")
    public EmployeeStatus createEmployeeStatus(
       CreateEmployeeStatusRequest request
    ) {
        if(repository.existsByStatus(request.getStatus())) {
            throw new DuplicateResourceException("EmployeeStatus", "status", request.getStatus());
        }

        EmployeeStatus status = statusMapper.toEntity(request);
        return repository.save(status);
    }

    public ExtendedEmployeeStatusDto findByIdDetailed(UUID id) {
        return repository.findByIdDetailed(id)
                         .orElseThrow(() -> new ResourceNotFoundException("EmployeeStatus", "id", id));
    }

    public Set<ExtendedEmployeeStatusDto> findAllDetailed() {
        return repository.findAllDetailed();
    }

    @Secured("ROLE_ADMIN")
    public ExtendedEmployeeStatusDto updateEmployeeStatus(
            UpdateEmployeeStatusRequest request
    ) {
        EmployeeStatus status = repository.findById(request.getEmployeeStatusId())
                                          .orElseThrow(() -> new ResourceNotFoundException("EmployeeStatus", "id", request.getEmployeeStatusId()));
        BeanUtils.copyProperties(request, status);
        repository.save(status);
        return repository.findByIdDetailed(request.getEmployeeStatusId())
                         .orElseThrow(() -> new ResourceNotFoundException("EmployeeStatus", "id", request.getEmployeeStatusId()));
    }

    @Secured("ROLE_ADMIN")
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void deleteEmployeeStatuses(DeleteEmployeeStatusRequest request) {
        Iterable<EmployeeStatus> statuses = repository.findAllById(request.getEmployeeStatuses());
        repository.deleteAll(statuses);
    }

}
