package ru.ompec.iam.service;

import lombok.SneakyThrows;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ru.ompec.iam.domain.Department;
import ru.ompec.iam.domain.Employee;
import ru.ompec.iam.domain.EmployeeStatus;
import ru.ompec.iam.exception.FailureUploadPhotoException;
import ru.ompec.iam.exception.ResourceNotFoundException;
import ru.ompec.iam.mapper.EmployeeMapper;
import ru.ompec.iam.payload.employee.CreateEmployeeRequest;
import ru.ompec.iam.payload.employee.UpdateEmployeeRequest;
import ru.ompec.iam.repository.DepartmentRepository;
import ru.ompec.iam.repository.EmployeeRepository;
import ru.ompec.iam.repository.EmployeeStatusRepository;


import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final DepartmentRepository departmentRepository;
    private final FileStorageService fileStorageService;
    private final EmployeeMapper employeeMapper;
    private final EmployeeStatusRepository statusRepository;

    @Autowired
    public EmployeeService(
            EmployeeRepository employeeRepository,
            EmployeeMapper employeeMapper,
            FileStorageService fileStorageService,
            DepartmentRepository departmentRepository,
            EmployeeStatusRepository statusRepository
    ) {
        this.employeeRepository = employeeRepository;
        this.departmentRepository = departmentRepository;
        this.fileStorageService = fileStorageService;
        this.employeeMapper = employeeMapper;
        this.statusRepository = statusRepository;
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public Employee createEmployee(CreateEmployeeRequest request) {
        Employee employee = employeeMapper.toEntity(request);
        return employeeRepository.save(employee);
    }

    public Employee getEmployeeById(UUID id) {
        Employee employee = employeeRepository.findFullEmployeeById(id)
                                              .orElseThrow(() -> new ResourceNotFoundException("Employee", "id", id));
        return employee;
    }

    public Page<Employee> getEmployeesByDepartment(Optional<UUID> departmentId, Pageable pageable) {
        Page<Employee> page;
        if(departmentId.isPresent()) {
            page = employeeRepository.findAllByDepartment_Id(departmentId.get(), pageable);
        } else {
            page = employeeRepository.findAll(pageable);
        }

        return page;
    }

    @Secured("ROLE_ADMIN")
    public void removeEmployee(UUID employeeID) {
        employeeRepository.deleteById(employeeID);
    }

    @Secured("ROLE_ADMIN")
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void removeEmployees(List<UUID> employeeIds) {
        Iterable<Employee> employees = employeeRepository.findAllById(employeeIds);
        employeeRepository.deleteAll(employees);
    }

    @Secured("ROLE_ADMIN")
    public Employee updateEmployee(UpdateEmployeeRequest request) {
        Employee employee = employeeRepository.findFullEmployeeById(request.getEmployeeId())
                                              .orElseThrow(() -> new ResourceNotFoundException("Employee", "id", request.getEmployeeId()));
        Department department = departmentRepository.findById(request.getDepartmentId())
                                                    .orElseThrow(() -> new ResourceNotFoundException("Department", "id", request.getDepartmentId()));
        EmployeeStatus status = statusRepository.findByIdFull(request.getEmployeeStatusId())
                                                .orElseThrow(() -> new ResourceNotFoundException("EmployeeStatus", "id", request.getEmployeeStatusId()));
        BeanUtils.copyProperties(request, employee);
        employee.setDepartment(department);
        employee.setStatus(status);
        return employeeRepository.save(employee);
    }

    @SneakyThrows
    @Secured("ROLE_ADMIN")
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void saveEmployeePhoto(
            MultipartFile photo,
            UUID employeeId
    ) {
        Employee employee = employeeRepository.findById(employeeId)
                                              .orElseThrow(() -> new ResourceNotFoundException("Employee", "id", employeeId));

        if(!Objects.isNull(employee.getPhotoFileName())) {
            fileStorageService.delete(employee.getPhotoFileName());
        }

        String photoFileName = UUID.randomUUID().toString() + photo.getOriginalFilename();
        try {
            fileStorageService.save(photo, photoFileName);
        } catch (IOException e) {
            throw new FailureUploadPhotoException();
        }
        employee.setPhotoFileName(photoFileName);
        employeeRepository.save(employee);
    }

    @SneakyThrows
    @Secured("ROLE_ADMIN")
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void deleteEmployeePhoto(
            UUID employeeId
    ) {
        Employee employee = employeeRepository.findById(employeeId)
                                              .orElseThrow(() -> new ResourceNotFoundException("Employee", "id", employeeId));
        if(employee.getPhotoFileName() != null && !employee.getPhotoFileName().isEmpty()) {
            fileStorageService.delete(employee.getPhotoFileName());
        }
        employee.setPhotoFileName(null);
        employeeRepository.save(employee);
    }

    public Page<Employee> findAll(Specification<Employee> spec, Pageable pageable) {
        return employeeRepository.findAll(spec, pageable);
    }

}
