package ru.ompec.iam.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import ru.ompec.iam.domain.Department;
import ru.ompec.iam.dto.response.ExtendedDepartmentDto;
import ru.ompec.iam.exception.DuplicateResourceException;
import ru.ompec.iam.exception.ResourceNotFoundException;
import ru.ompec.iam.mapper.DepartmentMapper;
import ru.ompec.iam.payload.department.CreateDepartmentRequest;
import ru.ompec.iam.payload.department.DeleteDepartmentsRequest;
import ru.ompec.iam.payload.department.UpdateDepartmentRequest;
import ru.ompec.iam.repository.DepartmentRepository;

import java.util.Set;
import java.util.UUID;

@Service
@Transactional
public class DepartmentService {

    private final DepartmentMapper mapper;
    private final DepartmentRepository departmentRepository;

    @Autowired
    public DepartmentService(DepartmentMapper mapper,
                             DepartmentRepository departmentRepository) {
        this.mapper = mapper;
        this.departmentRepository = departmentRepository;
    }

    @Secured("ROLE_ADMIN")
    public Department createDepartment(CreateDepartmentRequest request) {
        if(departmentRepository.existsByTitle(request.getTitle())) {
            throw new DuplicateResourceException("Department", "title", request.getTitle());
        }

        Department department = mapper.createDepartmentRequestToDepartment(request);
        return departmentRepository.save(department);
    }

    public ExtendedDepartmentDto findDepartmentByIdDetailed(UUID id) {
        return departmentRepository.findByIdDetailed(id)
                                   .orElseThrow(() -> new ResourceNotFoundException("Department", "id", id));
    }

    public Set<ExtendedDepartmentDto> findDepartmentsDetailed() {
        return departmentRepository.findAllDetailed();
    }

    @Secured("ROLE_ADMIN")
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public ExtendedDepartmentDto updateDepartment(UpdateDepartmentRequest request) {
        Department department = departmentRepository.findById(request.getDepartmentId())
                                                    .orElseThrow(() -> new ResourceNotFoundException("Department", "id", request.getDepartmentId()));
        BeanUtils.copyProperties(request, department);
        departmentRepository.save(department);
        return departmentRepository.findByIdDetailed(request.getDepartmentId()).orElseThrow(() -> new ResourceNotFoundException("Department", "id", request.getDepartmentId()));
    }

    @Secured("ROLE_ADMIN")
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void deleteDepartments(DeleteDepartmentsRequest request) {
        Iterable<Department> departments = departmentRepository.findAllById(request.getDepartments());
        departmentRepository.deleteAll(departments);
    }

}
