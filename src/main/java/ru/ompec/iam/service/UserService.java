package ru.ompec.iam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ompec.iam.domain.Role;
import ru.ompec.iam.domain.User;
import ru.ompec.iam.exception.ResourceNotFoundException;
import ru.ompec.iam.payload.user.CreateUserRequest;
import ru.ompec.iam.payload.user.UpdateUserPasswordRequest;
import ru.ompec.iam.payload.user.UpdateUserRequest;
import ru.ompec.iam.repository.RoleRepository;
import ru.ompec.iam.repository.UserRepository;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class UserService {

    private final UserRepository userRepository;
    private final AuthTokenService authTokenService;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;

    @Autowired
    public UserService(
            UserRepository userRepository,
            AuthTokenService authTokenService,
            PasswordEncoder passwordEncoder,
            RoleRepository roleRepository
    ) {
        this.userRepository = userRepository;
        this.authTokenService = authTokenService;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
    }

    public User findUserById(UUID id) {
        return userRepository.findById(id)
                             .orElseThrow(() -> new ResourceNotFoundException("User", "id", id));
    }

    @Secured("ROLE_ADMIN")
    public User createUser(
            CreateUserRequest request
    ) {
        Role role = roleRepository.findById(request.getRoleId())
                                  .orElseThrow(() -> new ResourceNotFoundException("Role", "id", request.getRoleId()));

        User user = new User();
        user.setUsername(request.getLogin()); // 0_o
        user.setPasswordHash(passwordEncoder.encode(request.getPassword()));
        user.addRole(role);
        user.setActive(request.getIsActive()); // 0_o

        userRepository.save(user);
        return user;
    }

    @Secured("ROLE_ADMIN")
    public void deleteUser(UUID id) {
        userRepository.deleteById(id);
    }

    @Secured("ROLE_ADMIN")
    public List<User> findAllUsers() {
        return userRepository.findAll();
    }

    @Secured("ROLE_ADMIN")
    public User updateUser(UpdateUserRequest request) {
        User user = userRepository.findById(request.getUserId())
                                  .orElseThrow(() -> new ResourceNotFoundException("User", "id", request.getUserId()));
        Role role = roleRepository.findById(request.getRoleId())
                                  .orElseThrow(() -> new ResourceNotFoundException("Role", "id", request.getRoleId()));
        user.setActive(request.getIsActive());
        user.getRoles().clear();
        user.addRole(role);
        return userRepository.save(user);
    }

    @Secured("ROLE_ADMIN")
    public User updateUserPassword(UpdateUserPasswordRequest request) {
        User user = userRepository.findById(request.getUserId())
                                  .orElseThrow(() -> new ResourceNotFoundException("User", "id", request.getUserId()));
        user.setPasswordHash(passwordEncoder.encode(request.getPassword()));
        // revoke all tokens
        user.getAuthTokens().clear();
        return userRepository.save(user);
    }

}
