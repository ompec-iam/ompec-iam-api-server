package ru.ompec.iam.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import ru.ompec.iam.domain.PermissionStatus;
import ru.ompec.iam.dto.response.ExtendedPermissionStatusDto;
import ru.ompec.iam.exception.DuplicateResourceException;
import ru.ompec.iam.exception.ResourceNotFoundException;
import ru.ompec.iam.mapper.PermissionStatusMapper;
import ru.ompec.iam.payload.permissionstatus.CreatePermissionStatusRequest;
import ru.ompec.iam.payload.permissionstatus.DeletePermissionStatusRequest;
import ru.ompec.iam.payload.permissionstatus.UpdatePermissionStatusRequest;
import ru.ompec.iam.repository.PermissionStatusRepository;

import java.util.Set;
import java.util.UUID;

@Service
@Transactional
public class PermissionStatusService {

    private final PermissionStatusRepository repository;
    private final PermissionStatusMapper mapper;

    @Autowired
    public PermissionStatusService(PermissionStatusRepository repository,
                                   PermissionStatusMapper mapper) {
        this.repository = repository;
        this.mapper = mapper;
    }

    @Secured("ROLE_ADMIN")
    public PermissionStatus createPermissionStatus(CreatePermissionStatusRequest request) {
        if(repository.existsByStatus(request.getStatus())) {
            throw new DuplicateResourceException("PermissionStatus", "status", request.getStatus());
        }

        PermissionStatus status = mapper.toEntity(request);
        return repository.save(status);
    }

    public ExtendedPermissionStatusDto findPermissionStatusByIdDetailed(UUID id) {
        return repository.findByIdDetailed(id)
                         .orElseThrow(() -> new ResourceNotFoundException("Permission status", "id", id));
    }

    public Set<ExtendedPermissionStatusDto> findAllDetailed() {
        return repository.findAllDetailed();
    }

    @Secured("ROLE_ADMIN")
    public ExtendedPermissionStatusDto updatePermissionStatus(
            UpdatePermissionStatusRequest request
    ) {
        PermissionStatus status = repository.findById(request.getPermissionStatusId())
                                            .orElseThrow(() -> new ResourceNotFoundException("Permission status", "id", request.getPermissionStatusId()));
        BeanUtils.copyProperties(request, status);
        repository.save(status);
        return repository.findByIdDetailed(request.getPermissionStatusId())
                         .orElseThrow(() -> new ResourceNotFoundException("Permission status", "id", request.getPermissionStatusId()));
    }

    @Secured("ROLE_ADMIN")
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void deletePermissionStatuses(DeletePermissionStatusRequest request) {
        Iterable<PermissionStatus> statuses = repository.findAllById(request.getPermissionStatuses());
        repository.deleteAll(statuses);
    }

}
