package ru.ompec.iam.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.ompec.iam.domain.Token;
import ru.ompec.iam.domain.User;
import ru.ompec.iam.exception.ResourceNotFoundException;
import ru.ompec.iam.repository.TokenRepository;
import ru.ompec.iam.repository.UserRepository;
import ua_parser.Client;
import ua_parser.Parser;

import javax.servlet.http.HttpServletRequest;
import java.util.Objects;
import java.util.UUID;


@Service
@Transactional(isolation = Isolation.SERIALIZABLE)
public class AuthTokenService {

    private final UserRepository userRepository;
    private final TokenRepository tokenRepository;
    private Parser parser;

    @Autowired
    public AuthTokenService(
            UserRepository userRepository,
            TokenRepository tokenRepository
    ) {
        this.userRepository = userRepository;
        this.tokenRepository = tokenRepository;
        this.parser = new Parser();
    }

    public Token saveToken(
            String authToken,
            String username,
            HttpServletRequest httpRequest
    ) {
        User user = userRepository.findUserByUsername(username)
                                  .orElseThrow(() -> new ResourceNotFoundException("User", "username", username));
        Token token = new Token();
        token.setToken(authToken);
        token.setUser(user);
        token.setIpAddress(extractIp(httpRequest));
        token.setDeviceInfo(getDeviceDetails(httpRequest));
        userRepository.save(user);
        return token;
    }

    public void logout(HttpServletRequest httpRequest) {
        String token = getJwtFromRequest(httpRequest);
        removeToken(token);
    }

    public boolean existByTokenValue(String token) {
        return tokenRepository.existsByToken(token);
    }

    public void removeToken(String token) {
        tokenRepository.deleteByToken(token);
    }

    @Secured("ROLE_ADMIN")
    public void removeTokenById(UUID id) {
        tokenRepository.deleteById(id);
    }

    private String extractIp(HttpServletRequest request) {
        String clientIp;
        String clientXForwardedForIp = request
                .getHeader("x-forwarded-for");
        if (Objects.nonNull(clientXForwardedForIp)) {
            clientIp = parseXForwardedHeader(clientXForwardedForIp);
        } else {
            clientIp = request.getRemoteAddr();
        }
        return clientIp;
    }

    private String parseXForwardedHeader(String header) {
        return header.split(" *, *")[0];
    }

    private String getDeviceDetails(HttpServletRequest request) {
        String userAgent = request.getHeader("user-agent");

        String deviceDetails = "Unknown";

        Client client = parser.parse(userAgent);
        if (Objects.nonNull(client)) {
            deviceDetails = client.userAgent.family + " " + client.userAgent.major + "." + client.userAgent.minor +
                    " - " + client.os.family + " " + client.os.major + "." + client.os.minor;
        }

        return deviceDetails;
    }

    private String getJwtFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7, bearerToken.length());
        }
        return null;
    }

}
