package ru.ompec.iam.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import ru.ompec.iam.domain.Permission;
import ru.ompec.iam.domain.PermissionStatus;
import ru.ompec.iam.exception.ResourceNotFoundException;
import ru.ompec.iam.mapper.PermissionMapper;
import ru.ompec.iam.payload.permission.CreatePermissionRequest;
import ru.ompec.iam.payload.permission.DeletePermissionRequest;
import ru.ompec.iam.payload.permission.UpdatePermissionRequest;
import ru.ompec.iam.repository.EmployeeRepository;
import ru.ompec.iam.repository.PermissionRepository;
import ru.ompec.iam.repository.PermissionStatusRepository;


import java.util.Optional;
import java.util.UUID;

@Service
@Transactional
public class PermissionService {

    private final PermissionMapper permissionMapper;
    private final PermissionRepository permissionRepository;
    private final PermissionStatusRepository statusRepository;

    @Autowired
    public PermissionService(PermissionMapper permissionMapper,
                             PermissionRepository permissionRepository,
                             PermissionStatusRepository statusRepository) {
        this.permissionMapper = permissionMapper;
        this.permissionRepository = permissionRepository;
        this.statusRepository = statusRepository;
    }

    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public Permission createPermission(CreatePermissionRequest request) {
        Permission permission = permissionMapper.toPermission(request);
        return permissionRepository.save(permission);
    }

    @Secured("ROLE_ADMIN")
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void removePermissions(DeletePermissionRequest request) {
        Iterable<Permission> permissions = permissionRepository.findAllById(request.getPermissions());
        permissionRepository.deleteAll(permissions);
    }

    public Page<Permission> getPermissionsByResource(Optional<UUID> resourceId, Pageable pageable) {
        Page<Permission> page = permissionRepository.findAll(pageable);
        return page;
    }

    public Page<Permission> findAll(Specification<Permission> spec, Pageable pageable) {
        return permissionRepository.findAll(spec, pageable);
    }

    @Secured("ROLE_ADMIN")
    public Permission updatePermission(UpdatePermissionRequest request) {
        Permission permission = permissionRepository.findById(request.getPermissionId())
                                                    .orElseThrow(() -> new ResourceNotFoundException("Permission", "id", request.getStatusId()));
        PermissionStatus status = statusRepository.findByIdFull(request.getStatusId())
                                                  .orElseThrow(() -> new ResourceNotFoundException("PermissionStatus", "id", request.getStatusId()));
        permission.setStatus(status);
        permission.getCredential().setPassword(request.getPassword());
        return permissionRepository.save(permission);
    }


}
