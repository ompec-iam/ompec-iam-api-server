package ru.ompec.iam.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import ru.ompec.iam.domain.Resource;
import ru.ompec.iam.dto.response.ExtendedResourceDto;
import ru.ompec.iam.exception.DuplicateResourceException;
import ru.ompec.iam.exception.ResourceNotFoundException;
import ru.ompec.iam.mapper.ResourceMapper;
import ru.ompec.iam.payload.resource.CreateResourceRequest;
import ru.ompec.iam.payload.resource.DeleteResourceRequest;
import ru.ompec.iam.payload.resource.UpdateResourceRequest;
import ru.ompec.iam.repository.ResourceRepository;

import java.util.Set;
import java.util.UUID;

@Service
@Transactional
public class ResourceService {

    private final ResourceMapper mapper;
    private final ResourceRepository resourceRepository;

    @Autowired
    public ResourceService(ResourceMapper mapper, ResourceRepository resourceRepository) {
        this.mapper = mapper;
        this.resourceRepository = resourceRepository;
    }

    @Secured("ROLE_ADMIN")
    public Resource createResource(CreateResourceRequest request) {
        if(resourceRepository.existsByTitle(request.getTitle())) {
            throw new DuplicateResourceException("Resource", "title", request.getTitle());
        }

        Resource resource = mapper.createResourceRequestToResource(request);
        return resourceRepository.save(resource);
    }

    public Set<ExtendedResourceDto> findAllDetailed() {
        return resourceRepository.findAllDetailed();
    }

    public ExtendedResourceDto findResourceByIdDetailed(UUID id) {
        return resourceRepository.findByIdDetailed(id)
                                 .orElseThrow(() -> new ResourceNotFoundException("Resource", "id", id));
    }

    @Secured("ROLE_ADMIN")
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public void deleteResources(DeleteResourceRequest request) {
        Iterable<Resource> resources = resourceRepository.findAllById(request.getResources());
        resourceRepository.deleteAll(resources);
    }

    @Secured("ROLE_ADMIN")
    @Transactional(isolation = Isolation.SERIALIZABLE)
    public ExtendedResourceDto updateResource(UpdateResourceRequest request) {
        Resource resource = resourceRepository.findById(request.getResourceId())
                                              .orElseThrow(() -> new ResourceNotFoundException("Resource", "id", request.getResourceId()));
        BeanUtils.copyProperties(request, resource);
        resourceRepository.save(resource);
        return resourceRepository.findByIdDetailed(request.getResourceId())
                                 .orElseThrow(() -> new ResourceNotFoundException("Resource", "id", request.getResourceId()));
    }

}
