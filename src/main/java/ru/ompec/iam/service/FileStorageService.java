package ru.ompec.iam.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileStorageService {

    public void init();

    public void save(MultipartFile file, String fileName) throws IOException;

    public void delete(String fileName) throws IOException;

    public Resource load(String filename);

}
