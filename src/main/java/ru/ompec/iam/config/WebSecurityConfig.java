package ru.ompec.iam.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import ru.ompec.iam.exception.handler.CustomAccessDeniedHandler;
import ru.ompec.iam.repository.UserRepository;
import ru.ompec.iam.security.JpaUserDetailsService;
import ru.ompec.iam.security.TokenAuthenticationFilter;
import ru.ompec.iam.security.TokenProvider;
import ru.ompec.iam.service.AuthTokenService;


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        securedEnabled = true,
        jsr250Enabled = true,
        prePostEnabled = true
)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final TokenProvider tokenProvider;
    private final JpaUserDetailsService jpaUserDetailsService;
    private final AccessDeniedHandler accessDeniedHandler;
    private final AuthTokenService authTokenService;

    @Autowired
    public WebSecurityConfig(TokenProvider tokenProvider,
                             JpaUserDetailsService jpaUserDetailsService,
                             AccessDeniedHandler accessDeniedHandler,
                             AuthTokenService authTokenService) {
        this.tokenProvider = tokenProvider;
        this.jpaUserDetailsService = jpaUserDetailsService;
        this.accessDeniedHandler = accessDeniedHandler;
        this.authTokenService = authTokenService;
    }

    @Bean
    public TokenAuthenticationFilter tokenAuthenticationFilter() {
        return new TokenAuthenticationFilter(tokenProvider, jpaUserDetailsService, authTokenService);
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(jpaUserDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Bean(BeanIds.AUTHENTICATION_MANAGER)
    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests(authorizeRequests ->
                    authorizeRequests
                            .mvcMatchers(HttpMethod.GET, "/api/user/me").hasAnyRole("USER", "ADMIN")
                            .mvcMatchers(HttpMethod.GET, "/api/employee/*").hasAnyRole("USER", "ADMIN")
                            .mvcMatchers(HttpMethod.GET, "/api/employees/**").hasAnyRole("USER", "ADMIN")
                            .mvcMatchers(HttpMethod.GET, "/api/department/*").hasAnyRole("USER", "ADMIN")
                            .mvcMatchers(HttpMethod.GET, "/api/departments").hasAnyRole("USER", "ADMIN")
                            .mvcMatchers(HttpMethod.GET, "/api/resources").hasAnyRole("USER", "ADMIN")
                            .mvcMatchers(HttpMethod.GET, "/api/resource/*").hasAnyRole("USER", "ADMIN")
                            .mvcMatchers(HttpMethod.GET, "/api/permission_status/*").hasAnyRole("USER", "ADMIN")
                            .mvcMatchers(HttpMethod.GET, "/api/employee_status/*").hasAnyRole("USER", "ADMIN")
                            .mvcMatchers(HttpMethod.GET, "/api/permission_statuses").hasAnyRole("USER", "ADMIN")
                            .mvcMatchers(HttpMethod.GET, "/api/employee_statuses").hasAnyRole("USER", "ADMIN")
                            .mvcMatchers(HttpMethod.GET, "/api/permissions").hasAnyRole("USER", "ADMIN")
                            .mvcMatchers(HttpMethod.GET, "/api/v2/employees").hasAnyRole("USER", "ADMIN")
                            .mvcMatchers(HttpMethod.GET, "/api/employee_photo/**").permitAll()
                            .mvcMatchers(HttpMethod.GET, "/api/user/*").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.GET, "/api/users").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.GET, "/api/roles").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.POST,"/api/auth/login").permitAll()
                            .mvcMatchers(HttpMethod.POST, "/api/department").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.POST, "/api/resource").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.POST, "/api/employee").hasAnyRole("USER", "ADMIN")
                            .mvcMatchers(HttpMethod.POST, "/api/employee/upload_photo/*").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.POST, "/api/permission_status").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.POST, "/api/employee_status").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.POST, "/api/permission").hasAnyRole("USER", "ADMIN")
                            .mvcMatchers(HttpMethod.POST, "/api/user").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.PUT, "/api/employee").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.PUT, "/api/permission").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.PUT, "/api/department").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.PUT, "/api/resource").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.PUT, "/api/permission_status").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.PUT, "/api/employee_status").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.PUT, "/api/user").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.PUT, "/api/user/change_password").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.DELETE, "/api/employee/*").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.DELETE, "/api/employee/delete_photo/*").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.DELETE, "/api/employee").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.DELETE, "/api/permission").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.DELETE, "/api/department").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.DELETE, "/api/resource").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.DELETE, "/api/permission_status").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.DELETE, "/api/employee_status").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.DELETE, "/api/user/*").hasRole("ADMIN")
                            .mvcMatchers(HttpMethod.DELETE, "/api/auth/token/*").hasRole("ADMIN")
                            .anyRequest().authenticated()
                )
                .csrf(cust -> cust.disable())
                .httpBasic(cust -> cust.disable())
                .formLogin(cust -> cust.disable())
                .sessionManagement(cust ->
                        cust.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .exceptionHandling(ex ->
                        ex
                                .authenticationEntryPoint(new HttpStatusEntryPoint(HttpStatus.UNAUTHORIZED))
                                .accessDeniedHandler(accessDeniedHandler)
                );
                
        http.cors();
                

        http
                .addFilterBefore(tokenAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

}
