package ru.ompec.iam.domain;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@NamedEntityGraphs({
        @NamedEntityGraph(
                name = "Employee.full",
                attributeNodes = {
                        @NamedAttributeNode(value = "department"),
                        @NamedAttributeNode(value = "status"),
                        @NamedAttributeNode(value = "permissions", subgraph = "Permission.extend")
                },
                subgraphs = {
                        @NamedSubgraph(
                                name = "Permission.extend",
                                attributeNodes = {
                                        @NamedAttributeNode(value = "status"),
                                        @NamedAttributeNode(value = "resource"),
                                        @NamedAttributeNode(value = "employee"),
                                        @NamedAttributeNode(value = "credential")
                                }
                        )
                }
        )
})
@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true, exclude = { "permissions" })
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = { "permissions" })
@Entity
@Table(name = "employee")
public class Employee extends AbstractIdentifiable {

    @Column(name = "first_name", length = 50, nullable = false)
    @Nationalized
    private String firstName;

    @Column(name = "last_name", length = 50, nullable = false)
    @Nationalized
    private String lastName;

    @Column(name = "patronymic", length = 250)
    @Nationalized
    private String patronymic;

    @Column(name = "position", length = 250)
    @Nationalized
    private String position;

    @Column(name = "phone_number", length = 12, unique = true)
    @Nationalized
    private String phoneNumber;

    @Column(name = "photo_file_name")
    @Nationalized
    private String photoFileName;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "department_id")
    private Department department;

    @OneToMany(
            mappedBy = "employee",
            fetch = FetchType.LAZY,
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    @Builder.Default
    private Set<Permission> permissions = new HashSet<>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "employee_status_id")
    private EmployeeStatus status;

    public void setDepartment(Department department) {
        this.department = department;
        department.getEmployees().add(this);
    }

    public void setStatus(EmployeeStatus status) {
        this.status = status;
        status.getEmployees().add(this);
    }

}
