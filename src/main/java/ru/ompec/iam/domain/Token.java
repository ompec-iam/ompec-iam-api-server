package ru.ompec.iam.domain;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;


@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true, of = { })
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "token")
public class Token extends AbstractIdentifiable {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "token", length = 2048, nullable = false, updatable = false)
    @Nationalized
    private String token;

    @Column(name = "ip_address")
    @Nationalized
    private String ipAddress;

    @Column(name = "device_info", length = 500)
    @Nationalized
    private String deviceInfo;

    public void setUser(User user) {
        this.user = user;
        user.getAuthTokens().add(this);
    }

}
