package ru.ompec.iam.domain;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;

@Data
@SuperBuilder
@EqualsAndHashCode(of = {},callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = { "permission" })
@Entity
@Table(name = "credential")
public class Credential extends AbstractIdentifiable {

    @Column(name = "login", unique = false, updatable = false, length = 35)
    @Nationalized
    private String login;

    @Column(name = "password")
    @Nationalized
    private String password;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "credential")
    private Permission permission;

}
