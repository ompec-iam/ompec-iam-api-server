package ru.ompec.iam.domain;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@NamedEntityGraphs({
        @NamedEntityGraph(
                name = "Permission.Full",
                attributeNodes = {
                        @NamedAttributeNode(value = "permissions")
                }
        )
})
@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true, exclude = { "permissions" })
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = { "permissions" })
@Entity
@Table(name = "permission_status")
public class PermissionStatus extends AbstractIdentifiable {

    @Column(name = "status", length = 40, unique = true, nullable = false, updatable = false)
    @Nationalized
    private String status;

    @Column(name = "color")
    @Nationalized
    private String color;

    @OneToMany(mappedBy = "status", fetch = FetchType.LAZY, orphanRemoval = true, cascade = CascadeType.ALL)
    @Builder.Default
    private Set<Permission> permissions = new HashSet<>();

}
