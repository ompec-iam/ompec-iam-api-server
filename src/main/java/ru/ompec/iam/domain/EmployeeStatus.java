package ru.ompec.iam.domain;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Nationalized;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@NamedEntityGraphs({
        @NamedEntityGraph(
                name = "EmployeeStatus.Full",
                attributeNodes = {
                        @NamedAttributeNode(value = "employees")
                }
        )
})
@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true, exclude = { "employees" })
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = { "employees" })
@Entity
@Table(name = "employee_status")
public class EmployeeStatus extends AbstractIdentifiable {

    @Column(name = "status", length = 40, unique = true, nullable = false, updatable = false)
    @Nationalized
    private String status;

    @Column(name = "color")
    @Nationalized
    private String color;

    @OneToMany(
            mappedBy = "status",
            fetch = FetchType.LAZY,
            orphanRemoval = true,
            cascade = CascadeType.ALL
    )
    @Builder.Default
    private Set<Employee> employees = new HashSet<>();

}
