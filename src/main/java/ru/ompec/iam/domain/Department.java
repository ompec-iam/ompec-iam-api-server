package ru.ompec.iam.domain;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Nationalized;


import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Data
@SuperBuilder
@EqualsAndHashCode(callSuper = true, of = { "title" })
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = { "employees" })
@Entity
@Table(name = "department")
public class Department extends AbstractIdentifiable {

    @Column(name = "title", length = 250, unique = true)
    @Nationalized
    private String title;

    @OneToMany(
            mappedBy = "department",
            fetch = FetchType.LAZY,
            orphanRemoval = false,
            cascade = {
            CascadeType.DETACH,
            CascadeType.MERGE,
            CascadeType.PERSIST,
            CascadeType.REFRESH,
            CascadeType.REMOVE
    })
    @Builder.Default
    private Set<Employee> employees = new HashSet<>();

}
