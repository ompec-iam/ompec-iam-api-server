package ru.ompec.iam.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.time.LocalDateTime;

@NamedEntityGraphs({
        @NamedEntityGraph(
                name = "Permission.full",
                attributeNodes = {
                        @NamedAttributeNode(value = "status"),
                        @NamedAttributeNode(value = "resource"),
                        @NamedAttributeNode(value = "employee"),
                        @NamedAttributeNode(value = "credential")
                }
        )
})
@Data
@SuperBuilder
@EqualsAndHashCode(of = {},callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString(exclude = { "employee" })
@Entity
@Table(name = "permission")
public class Permission extends AbstractIdentifiable {

    @Column(name = "creation_date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime creationDate;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "permission_status_id")
    private PermissionStatus status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "resource_id")
    private Resource resource;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "employee_id")
    private Employee employee;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name = "permission_credential",
    joinColumns = {
            @JoinColumn(name = "permission_id", referencedColumnName = "id")
    },
    inverseJoinColumns = {
            @JoinColumn(name = "credential_id", referencedColumnName = "id")
    })
    private Credential credential;

    public void setCredential(Credential credential) {
        this.credential = credential;
        credential.setPermission(this);
    }

    public void setStatus(PermissionStatus status) {
        this.status = status;
        status.getPermissions().add(this);
    }

    public void setResource(Resource resource) {
        this.resource = resource;
        resource.getPermissions().add(this);
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        employee.getPermissions().add(this);
    }

}
