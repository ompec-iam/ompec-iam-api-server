package ru.ompec.iam.validation.constraintvalidation.common;

import ru.ompec.iam.validation.constraints.common.NotSpace;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NotSpaceValidator implements ConstraintValidator<NotSpace, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if(value == null) {
            return true;
        }
        return !value.contains(" ");
    }

}
