package ru.ompec.iam.validation.constraintvalidation.common;

import ru.ompec.iam.validation.constraints.common.OnlyCharacters;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class OnlyCharactersValidator implements ConstraintValidator<OnlyCharacters, String> {

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if(value == null) {
            return true;
        }

        for(int k = 0; k < value.length(); k++) {
            char ch = value.charAt(k);

            if(!Character.isSpaceChar(ch) && !Character.isLetter(ch)) {
                return false;
            }
        }

        return true;
    }
}
