package ru.ompec.iam.validation.constraintvalidation.common;

import ru.ompec.iam.validation.constraints.common.EmployeePassword;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EmployeePasswordValidator implements ConstraintValidator<EmployeePassword, String> {

    private final char[] requiredCharacters = new char[] {
            '.', '/', '*', '[', ']', '_', '@', '?'
    };

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if(value == null) {
            return true;
        }

        if(value.equals(value.toLowerCase())) {
            return false;
        }
        if(value.equals(value.toUpperCase())) {
            return false;
        }

        boolean isContainsRequiredCharacters = false;

        for(char ch : value.toCharArray()) {
            for(char required : requiredCharacters) {
                if (ch == required) {
                    isContainsRequiredCharacters = true;
                    break;
                }
            }
        }

        return isContainsRequiredCharacters;
    }

}
