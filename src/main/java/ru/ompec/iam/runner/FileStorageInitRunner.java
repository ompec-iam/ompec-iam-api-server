package ru.ompec.iam.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import ru.ompec.iam.service.FileStorageService;

@Component
public class FileStorageInitRunner implements ApplicationRunner {

    private final FileStorageService fileStorageService;

    @Autowired
    public FileStorageInitRunner(FileStorageService fileStorageService) {
        this.fileStorageService = fileStorageService;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        fileStorageService.init();
    }

}
