package ru.ompec.iam;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import ru.ompec.iam.config.ApplicationProperties;
import ru.ompec.iam.config.FileStorageConfig;

@EnableConfigurationProperties({
		ApplicationProperties.class,
		FileStorageConfig.class

})
@SpringBootApplication
public class OmpecIamApplication {

	public static void main(String[] args) {
		SpringApplication.run(OmpecIamApplication.class, args);
	}

}
