package ru.ompec.iam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.ompec.iam.domain.Department;
import ru.ompec.iam.dto.response.ExtendedDepartmentDto;


import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, UUID> {

    boolean existsByTitle(String title);

    @Query(value = "select d.id as id, d.title as title, (select count(e.id) from Employee e where e.department.id = :id) as count from Department d where d.id = :id")
    Optional<ExtendedDepartmentDto> findByIdDetailed(@Param("id") UUID id);

    @Query(value = "select d.id as id, d.title as title, (select count(e.id) from Employee e where e.department.id = d.id) as count from Department d")
    Set<ExtendedDepartmentDto> findAllDetailed();

}
