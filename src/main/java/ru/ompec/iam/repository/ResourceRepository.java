package ru.ompec.iam.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.ompec.iam.domain.Resource;
import ru.ompec.iam.dto.response.ExtendedResourceDto;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Repository
public interface ResourceRepository extends JpaRepository<Resource, UUID> {

    boolean existsByTitle(String title);

    @Query(value = "select r.id as id, r.title as title, (select count(p.id) from Permission p where p.resource.id = :id) as count from Resource r where r.id = :id")
    Optional<ExtendedResourceDto> findByIdDetailed(@Param("id") UUID id);

    @Query(value = "select r.id as id, r.title as title, (select count(p.id) from Permission p where p.resource.id = r.id) as count from Resource r")
    Set<ExtendedResourceDto> findAllDetailed();

    @EntityGraph(value = "Resource.Full", type = EntityGraph.EntityGraphType.FETCH)
    @Query(value = "select r from Resource r where r.id = :id")
    Optional<Resource> findByIdFull(@Param("id") UUID id);

}
