package ru.ompec.iam.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.ompec.iam.domain.EmployeeStatus;
import ru.ompec.iam.dto.response.ExtendedEmployeeStatusDto;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Repository
public interface EmployeeStatusRepository extends JpaRepository<EmployeeStatus, UUID> {

    boolean existsByStatus(String status);

    @Query(value = "select s.id as id, s.status as status, s.color as color, (select count (e.id) from Employee e where e.status.id = :id) as count from EmployeeStatus s where s.id = :id")
    Optional<ExtendedEmployeeStatusDto> findByIdDetailed(@Param("id") UUID id);

    @Query(value = "select s.id as id, s.status as status, s.color as color, (select count(e.id) from Employee e where e.status.id = s.id) as count from EmployeeStatus s")
    Set<ExtendedEmployeeStatusDto> findAllDetailed();

    @EntityGraph(value = "EmployeeStatus.Full", type = EntityGraph.EntityGraphType.FETCH)
    @Query(value = "select e from EmployeeStatus e where e.id = :id")
    Optional<EmployeeStatus> findByIdFull(@Param("id") UUID id);

}
