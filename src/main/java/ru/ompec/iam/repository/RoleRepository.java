package ru.ompec.iam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.ompec.iam.domain.Role;

import java.util.UUID;

@Repository
public interface RoleRepository  extends JpaRepository<Role, UUID> {
}
