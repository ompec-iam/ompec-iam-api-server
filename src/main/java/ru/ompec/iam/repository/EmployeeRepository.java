package ru.ompec.iam.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.ompec.iam.domain.Employee;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, UUID>, JpaSpecificationExecutor<Employee> {

    @EntityGraph(value = "Employee.full", type = EntityGraph.EntityGraphType.FETCH)
    @Query("select e from Employee e where e.id = :id")
    Optional<Employee> findFullEmployeeById(@Param("id") UUID id);

    @EntityGraph(value = "Employee.full", type = EntityGraph.EntityGraphType.FETCH)
    Page<Employee> findAllByDepartment_Id(UUID id, Pageable pageable);

    @Override
    @EntityGraph(value = "Employee.full", type = EntityGraph.EntityGraphType.FETCH)
    Page<Employee> findAll(Pageable pageable);

    @EntityGraph(value = "Employee.full", type = EntityGraph.EntityGraphType.FETCH)
    @Query(value = "select e from Employee e where e.id = :id")
    Optional<Employee> findByIdFull(@Param("id") UUID id);

    @Override
    @EntityGraph(value = "Employee.full", type = EntityGraph.EntityGraphType.FETCH)
    Page<Employee> findAll(Specification<Employee> spec, Pageable pageable);
}
