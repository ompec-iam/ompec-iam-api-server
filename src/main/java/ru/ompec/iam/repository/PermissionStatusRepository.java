package ru.ompec.iam.repository;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.ompec.iam.domain.PermissionStatus;
import ru.ompec.iam.dto.response.ExtendedPermissionStatusDto;

import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Repository
public interface PermissionStatusRepository extends JpaRepository<PermissionStatus, UUID> {

    boolean existsByStatus(String status);

    @EntityGraph(value = "Permission.Full", type = EntityGraph.EntityGraphType.FETCH)
    @Query("select p from PermissionStatus p where p.id = :id")
    Optional<PermissionStatus> findByIdFull(@Param("id") UUID id);

    @Query(value = "select s.id as id, s.status as status, s.color as color, (select count(p.id) from Permission p where p.status.id = :id) as count from PermissionStatus s where s.id = :id")
    Optional<ExtendedPermissionStatusDto> findByIdDetailed(@Param("id") UUID id);

    @Query(value = "select s.id as id, s.status as status, s.color as color, (select count(p.id) from Permission p where p.status.id = s.id) as count from PermissionStatus s")
    Set<ExtendedPermissionStatusDto> findAllDetailed();

}
