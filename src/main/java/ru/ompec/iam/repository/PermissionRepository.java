package ru.ompec.iam.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import ru.ompec.iam.domain.Permission;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, UUID>, JpaSpecificationExecutor<Permission> {

    @Override
    @EntityGraph(value = "Permission.full", type = EntityGraph.EntityGraphType.FETCH)
    Page<Permission> findAll(Specification<Permission> spec, Pageable pageable);

    @Override
    @EntityGraph(value = "Permission.full", type = EntityGraph.EntityGraphType.FETCH)
    Optional<Permission> findById(UUID uuid);
}
