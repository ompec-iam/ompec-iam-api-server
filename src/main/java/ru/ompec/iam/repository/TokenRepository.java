package ru.ompec.iam.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.ompec.iam.domain.Token;

import java.util.UUID;

@Repository
@Transactional
public interface TokenRepository extends JpaRepository<Token, UUID> {

    boolean existsByToken(String token);

    Token findByToken(String token);

    void deleteByToken(String token);

}
