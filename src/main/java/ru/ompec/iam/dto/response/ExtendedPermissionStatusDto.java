package ru.ompec.iam.dto.response;

import java.util.UUID;

public interface ExtendedPermissionStatusDto {

    UUID getId();
    String getStatus();
    String getColor();
    Long getCount();

}
