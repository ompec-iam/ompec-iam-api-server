package ru.ompec.iam.dto.response;

import java.util.UUID;

public interface ExtendedResourceDto {

    UUID getId();
    String getTitle();
    Long getCount();

}
