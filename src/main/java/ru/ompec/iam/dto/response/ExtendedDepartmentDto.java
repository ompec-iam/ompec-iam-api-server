package ru.ompec.iam.dto.response;

import java.util.UUID;

public interface ExtendedDepartmentDto {

    UUID getId();
    String getTitle();
    Long getCount();

}
