package ru.ompec.iam.pagination.specification;

import org.springframework.data.jpa.domain.Specification;
import ru.ompec.iam.domain.Department;
import ru.ompec.iam.domain.Employee;
import ru.ompec.iam.domain.EmployeeStatus;
import ru.ompec.iam.pagination.criteria.EmployeeCriteria;

import javax.persistence.criteria.*;
import java.util.UUID;

public class EmployeeSpecification implements Specification<Employee> {

    private EmployeeCriteria criteria;

    public EmployeeSpecification(final EmployeeCriteria criteria) {
        this.criteria = criteria;
    }


    @Override
    public Predicate toPredicate(
            Root<Employee> root,
            CriteriaQuery<?> query,
            CriteriaBuilder criteriaBuilder
    ) {
        switch(criteria.getKey()) {
            case "firstName":
                return criteriaBuilder.equal(root.get("firstName"), criteria.getValue());
            case "lastName":
                return criteriaBuilder.equal(root.get("lastName"), criteria.getValue());
            case "patronymic":
                return criteriaBuilder.equal(root.get("patronymic"), criteria.getValue());
            case "position":
                return criteriaBuilder.equal(root.get("position"), criteria.getValue());
            case "departmentId":
                Join<Employee, Department> departmentJoin = root.join("department");
                return criteriaBuilder.equal(departmentJoin.get("id"), UUID.fromString(criteria.getValue()));
            case "statusId":
                Join<Employee, EmployeeStatus> statusJoin = root.join("status");
                return criteriaBuilder.equal(statusJoin.get("id"), UUID.fromString(criteria.getValue()));
            default:
                return null;
        }
    }
}
