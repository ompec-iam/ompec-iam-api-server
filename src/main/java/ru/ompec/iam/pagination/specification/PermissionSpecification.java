package ru.ompec.iam.pagination.specification;

import org.springframework.data.jpa.domain.Specification;
import ru.ompec.iam.domain.Permission;
import ru.ompec.iam.domain.PermissionStatus;
import ru.ompec.iam.domain.Resource;
import ru.ompec.iam.pagination.criteria.PermissionCriteria;

import javax.persistence.criteria.*;


public class PermissionSpecification implements Specification<Permission> {

    private PermissionCriteria criteria;

    public PermissionSpecification(final PermissionCriteria criteria) {
        this.criteria = criteria;
    }

    @Override
    public Predicate toPredicate(
            Root<Permission> root,
            CriteriaQuery<?> query,
            CriteriaBuilder criteriaBuilder
    ) {
        switch (criteria.getKey()) {
            case "resourceId":
                Join<Permission, Resource> resourceJoin = root.join("resource");
                return criteriaBuilder.equal(resourceJoin.get("id"), criteria.getValue());
            case "statusId":
                Join<Permission, PermissionStatus> statusJoin = root.join("status");
                return criteriaBuilder.equal(statusJoin.get("id"), criteria.getValue());
            default:
                return null;
        }
    }
}
