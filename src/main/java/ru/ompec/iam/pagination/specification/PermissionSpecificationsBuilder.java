package ru.ompec.iam.pagination.specification;

import org.springframework.data.jpa.domain.Specification;
import ru.ompec.iam.domain.Permission;
import ru.ompec.iam.pagination.criteria.PermissionCriteria;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class PermissionSpecificationsBuilder {

    private final List<PermissionCriteria> params;

    public PermissionSpecificationsBuilder() {
        params = new ArrayList<>();
    }

    public PermissionSpecificationsBuilder with(String key, UUID value) {
        params.add(new PermissionCriteria(key, value));
        return this;
    }

    public Specification<Permission> build() {
        if (params.size() == 0) {
            return null;
        }

        List<Specification<Permission>> specs = params.stream()
                                          .map(PermissionSpecification::new)
                                          .collect(Collectors.toList());

        Specification<Permission> result = specs.get(0);

        for (int i = 1; i < params.size(); i++) {
            result = Specification.where(result).and(specs.get(i));
        }
        return result;
    }

}
