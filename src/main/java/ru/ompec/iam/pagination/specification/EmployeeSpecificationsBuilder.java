package ru.ompec.iam.pagination.specification;

import org.springframework.data.jpa.domain.Specification;
import ru.ompec.iam.domain.Employee;
import ru.ompec.iam.pagination.criteria.EmployeeCriteria;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EmployeeSpecificationsBuilder {

    private final List<EmployeeCriteria> params;

    public EmployeeSpecificationsBuilder() {
        params = new ArrayList<>();
    }

    public EmployeeSpecificationsBuilder with(String key, String value) {
        params.add(new EmployeeCriteria(key, value));
        return this;
    }

    public Specification<Employee> build() {
        if (params.size() == 0) {
            return null;
        }

        List<Specification<Employee>> specs = params.stream()
                                                    .map(EmployeeSpecification::new)
                                                    .collect(Collectors.toList());

        Specification<Employee> result = specs.get(0);

        for (int i = 1; i < params.size(); i++) {
            result = Specification.where(result).and(specs.get(i));
        }
        return result;
    }

}
