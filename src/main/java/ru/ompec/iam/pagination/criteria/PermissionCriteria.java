package ru.ompec.iam.pagination.criteria;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class PermissionCriteria {

    private String key;
    private UUID value;

}
