package ru.ompec.iam.pagination.criteria;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class EmployeeCriteria {

    private String key;
    private String value;

}
