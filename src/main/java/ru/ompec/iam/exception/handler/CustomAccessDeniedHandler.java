package ru.ompec.iam.exception.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;
import ru.ompec.iam.dto.response.ApiError;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Locale;

@Component
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

    private final MessageSource messageSource;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    public CustomAccessDeniedHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        ApiError error = new ApiError();
        error.setStatus(HttpStatus.FORBIDDEN);

        String errorMessage = "Access Denied";//messageSource.getMessage("access.denied", null, request.getLocale());
        error.setMessage(errorMessage);

        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        String responseBody = objectMapper.writeValueAsString(error);
        response.getOutputStream().println(responseBody);
    }
}
