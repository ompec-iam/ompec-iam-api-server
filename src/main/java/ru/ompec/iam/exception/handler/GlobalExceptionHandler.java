package ru.ompec.iam.exception.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import ru.ompec.iam.dto.response.ApiError;
import ru.ompec.iam.dto.response.ApiSubError;
import ru.ompec.iam.dto.response.ApiValidationError;
import ru.ompec.iam.exception.DuplicateResourceException;
import ru.ompec.iam.exception.FailureUploadPhotoException;
import ru.ompec.iam.exception.ResourceNotFoundException;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;


@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private final MessageSource messageSource;

    @Autowired
    public GlobalExceptionHandler(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<?> handleResourceNotFoundException(ResourceNotFoundException ex, Locale locale) {
        ApiError error = new ApiError(HttpStatus.NOT_FOUND);
        String errorMessage = messageSource.getMessage("resource.not_round", new Object[]{ex.getResourceName(), ex.getFieldName(), ex.getFieldValue()}, locale);
        error.setMessage(errorMessage);

        return new ResponseEntity<>(error, error.getStatus());
    }

    @ExceptionHandler(DuplicateResourceException.class)
    public ResponseEntity<?> handleDuplicateResourceException(DuplicateResourceException ex, Locale locale) {
        ApiError error = new ApiError(HttpStatus.CONFLICT);
        error.setMessage(messageSource.getMessage("resource.duplicate", new Object[]{ex.getResourceName(), ex.getFieldName(), ex.getFieldValue()}, locale));

        return new ResponseEntity<>(error, error.getStatus());
    }

    @ExceptionHandler(AccessDeniedException.class)
    public final ResponseEntity<ApiError> handleAccessDeniedException(AccessDeniedException ex, Locale locale) {
        ApiError error = new ApiError();
        error.setStatus(HttpStatus.FORBIDDEN);
        error.setMessage(messageSource.getMessage("access.denied", null ,locale));

        return new ResponseEntity<>(error, error.getStatus());
    }

    @ExceptionHandler(FailureUploadPhotoException.class)
    public final ResponseEntity<ApiError> handleFailureUploadPhotoException(Locale locale) {
        ApiError error = new ApiError();
        error.setStatus(HttpStatus.CONFLICT);

        error.setMessage(messageSource.getMessage("upload.failure", null ,locale));
        return new ResponseEntity<>(error, error.getStatus());
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        ApiError error = new ApiError();
        error.setStatus(HttpStatus.BAD_REQUEST);

        List<ApiSubError> subErrors =  ex.getBindingResult()
                                         .getFieldErrors()
                                         .stream()
                                         .map(e -> ApiValidationError.builder()
                                                                     .message(e.getDefaultMessage())
                                                                     .object(e.getObjectName())
                                                                     .rejectedValue(e.getRejectedValue())
                                                                     .field(e.getField())
                                                                     .build())
                                         .collect(Collectors.toList());

        error.setSubErrors(subErrors);

        return new ResponseEntity<>(error, error.getStatus());
    }

}
