package ru.ompec.iam.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.ompec.iam.domain.Resource;
import ru.ompec.iam.dto.response.ExtendedResourceDto;
import ru.ompec.iam.mapper.ResourceMapper;
import ru.ompec.iam.payload.resource.*;
import ru.ompec.iam.service.ResourceService;

import javax.validation.Valid;
import java.net.URI;
import java.util.Set;
import java.util.UUID;

@RestController
public class ResourceController {

    private final ResourceService resourceService;
    private final ResourceMapper resourceMapper;

    @Autowired
    public ResourceController(ResourceService resourceService,
                              ResourceMapper resourceMapper) {
        this.resourceService = resourceService;
        this.resourceMapper = resourceMapper;
    }

    @PostMapping("/api/resource")
    public ResponseEntity<?> createResource(@Valid @RequestBody CreateResourceRequest request) {
        Resource resource = resourceService.createResource(request);
        URI location = URI.create("/api/resource/" + resource.getId());
        ResourceResponse response = resourceMapper.toResponse(resource);

        return ResponseEntity.created(location).body(response);
    }

    @GetMapping("/api/resources")
    public ResponseEntity<?> getResources() {
        Set<ExtendedResourceDto> dtos = resourceService.findAllDetailed();
        Set<ExtendedResourceResponse> responses = resourceMapper.toDetailedResponses(dtos);
        ResourcesResponse response = new ResourcesResponse(responses);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/api/resource/{id}")
    public ResponseEntity<?> getResource(@PathVariable("id") UUID id) {
        ExtendedResourceDto dto = resourceService.findResourceByIdDetailed(id);
        ExtendedResourceResponse response = resourceMapper.toDetailedResponse(dto);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/api/resource")
    public ResponseEntity<?> deleteResources(@Valid @RequestBody DeleteResourceRequest request) {
        resourceService.deleteResources(request);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/api/resource")
    public ResponseEntity<?> updateResource(
            @Valid @RequestBody UpdateResourceRequest request
    ) {
        ExtendedResourceDto dto = resourceService.updateResource(request);
        ExtendedResourceResponse response = resourceMapper.toDetailedResponse(dto);

        return ResponseEntity.ok(response);
    }

}
