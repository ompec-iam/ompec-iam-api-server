package ru.ompec.iam.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.ompec.iam.dto.response.AuthResponse;
import ru.ompec.iam.payload.auth.LoginRequest;
import ru.ompec.iam.security.CurrentUser;
import ru.ompec.iam.security.TokenProvider;
import ru.ompec.iam.security.UserPrincipal;
import ru.ompec.iam.service.AuthTokenService;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;


@RestController
public class AuthController {

    private final AuthenticationManager authenticationManager;
    private final TokenProvider tokenProvider;
    private final AuthTokenService authTokenService;

    @Autowired
    public AuthController(
            AuthenticationManager authenticationManager,
            TokenProvider tokenProvider,
            AuthTokenService authTokenService
    ) {
        this.authenticationManager = authenticationManager;
        this.tokenProvider = tokenProvider;
        this.authTokenService = authTokenService;
    }

    @PostMapping("/api/auth/login")
    public ResponseEntity<?> login(
            @RequestBody LoginRequest request,
            HttpServletRequest httpRequest
    ) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getUsername(),
                        request.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String token = tokenProvider.createToken(authentication);
        authTokenService.saveToken(token, request.getUsername(), httpRequest);
        return ResponseEntity.ok(new AuthResponse(token));
    }

    @GetMapping("/test")
    public ResponseEntity<?> test(@CurrentUser UserPrincipal userPrincipal) {
        return ResponseEntity.ok(userPrincipal.getUsername());
    }

    @DeleteMapping("/api/auth/token/{id}")
    public ResponseEntity<?> deleteAuthToken(@PathVariable("id") UUID id) {
        authTokenService.removeTokenById(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/api/auth/logout")
    public ResponseEntity<?> logout(HttpServletRequest httpRequest) {
        authTokenService.logout(httpRequest);
        return ResponseEntity.ok().build();
    }

}
