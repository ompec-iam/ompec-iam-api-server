package ru.ompec.iam.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ompec.iam.domain.Role;
import ru.ompec.iam.mapper.RoleMapper;
import ru.ompec.iam.payload.role.RoleResponse;
import ru.ompec.iam.payload.role.RolesResponse;
import ru.ompec.iam.repository.RoleRepository;

import java.util.List;

@RestController
public class RoleController {

    private final RoleRepository roleRepository;
    private final RoleMapper roleMapper;

    @Autowired
    public RoleController(
            RoleRepository roleRepository,
            RoleMapper roleMapper
    ) {
        this.roleRepository = roleRepository;
        this.roleMapper = roleMapper;
    }

    @GetMapping("/api/roles")
    public ResponseEntity<?> getAllRoles() {
        List<Role> roles = roleRepository.findAll();
        List<RoleResponse> responses = roleMapper.toResponses(roles);

        return ResponseEntity.ok(new RolesResponse(responses));
    }

}
