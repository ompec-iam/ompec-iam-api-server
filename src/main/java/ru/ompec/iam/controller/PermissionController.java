package ru.ompec.iam.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.ompec.iam.domain.Permission;
import ru.ompec.iam.mapper.PermissionMapper;
import ru.ompec.iam.pagination.criteria.PermissionCriteria;
import ru.ompec.iam.pagination.specification.PermissionSpecification;
import ru.ompec.iam.pagination.specification.PermissionSpecificationsBuilder;
import ru.ompec.iam.payload.permission.CreatePermissionRequest;
import ru.ompec.iam.payload.permission.DeletePermissionRequest;
import ru.ompec.iam.payload.permission.PermissionResponse;
import ru.ompec.iam.payload.permission.UpdatePermissionRequest;
import ru.ompec.iam.service.PermissionService;

import javax.validation.Valid;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@RestController
public class PermissionController {

    private final PermissionService permissionService;
    private final PermissionMapper permissionMapper;

    @Autowired
    public PermissionController(PermissionService permissionService,
                                PermissionMapper permissionMapper) {
        this.permissionService = permissionService;
        this.permissionMapper = permissionMapper;
    }

    @PostMapping("/api/permission")
    public ResponseEntity<?> createPermission(@Valid @RequestBody CreatePermissionRequest request) {
        Permission permission = permissionService.createPermission(request);
        URI location = URI.create("/api/permission/" + permission.getId());
        PermissionResponse response = permissionMapper.toResponse(permission);

        return ResponseEntity.created(location).body(response);
    }

    @DeleteMapping("/api/permission")
    public ResponseEntity<?> deletePermissions(
            @Valid @RequestBody DeletePermissionRequest request
    ) {
        permissionService.removePermissions(request);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/api/permissions")
    public ResponseEntity<?> getPermissions(
            @RequestParam(value = "filter", required = false) Optional<String> filter,
            @PageableDefault(
                    size = 10,
                    sort = {
                            "resource.title",
                            "status.status",
                            "creationDate"
                    },
                    direction = Sort.Direction.ASC
            ) Pageable pageable
    ) {
        PermissionSpecificationsBuilder builder = new PermissionSpecificationsBuilder();

        if(filter.isPresent()) {
            //Pattern pattern = Pattern.compile("(\\w+?)(:)(\\w+?),");
            //Matcher matcher = pattern.matcher(filter + ",");
            String[] params = filter.get().split(",");

            for(String param : params) {
                String[] payload = param.split(":");
                builder.with(payload[0], UUID.fromString(payload[1]));
            }
        }

        Specification<Permission> spec = builder.build();

        Page<PermissionResponse> page = permissionService.findAll(spec, pageable)
                                                         .map(permissionMapper::toResponse);
        return ResponseEntity.ok(page);
    }

    @PutMapping("/api/permission")
    public ResponseEntity<?> updatePermission(
            @Valid @RequestBody UpdatePermissionRequest request
    ) {
        Permission permission = permissionService.updatePermission(request);
        PermissionResponse response = permissionMapper.toResponse(permission);

        return ResponseEntity.ok(response);
    }

}
