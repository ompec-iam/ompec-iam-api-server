package ru.ompec.iam.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.ompec.iam.domain.PermissionStatus;
import ru.ompec.iam.dto.response.ExtendedPermissionStatusDto;
import ru.ompec.iam.mapper.PermissionStatusMapper;
import ru.ompec.iam.payload.permissionstatus.*;
import ru.ompec.iam.service.PermissionStatusService;

import javax.validation.Valid;
import java.net.URI;
import java.util.Set;
import java.util.UUID;

@RestController
public class PermissionStatusController {

    private final PermissionStatusService permissionStatusService;
    private final PermissionStatusMapper statusMapper;

    @Autowired
    public PermissionStatusController(PermissionStatusService permissionStatusService,
                                      PermissionStatusMapper statusMapper) {
        this.permissionStatusService = permissionStatusService;
        this.statusMapper = statusMapper;
    }

    @PostMapping("/api/permission_status")
    public ResponseEntity<?> createPermissionStatus(@Valid @RequestBody CreatePermissionStatusRequest request) {
        PermissionStatus status = permissionStatusService.createPermissionStatus(request);
        URI location = URI.create("/api/permission_status/" + status.getId());
        PermissionStatusResponse response = statusMapper.toResponse(status);

        return ResponseEntity.created(location).body(response);
    }

    @GetMapping("/api/permission_status/{id}")
    public ResponseEntity<?> getPermissionStatus(@PathVariable("id") UUID id) {
        ExtendedPermissionStatusDto status = permissionStatusService.findPermissionStatusByIdDetailed(id);
        ExtendedPermissionStatusResponse response = statusMapper.toDetailedResponse(status);

        return ResponseEntity.ok(response);
    }

    @GetMapping("/api/permission_statuses")
    public ResponseEntity<?> getPermissionStatuses() {
        Set<ExtendedPermissionStatusDto> dtos = permissionStatusService.findAllDetailed();
        Set<ExtendedPermissionStatusResponse> statuses = statusMapper.toDetailedResponses(dtos);
        PermissionStatusesResponse response = new PermissionStatusesResponse(statuses);

        return ResponseEntity.ok(response);
    }

    @PutMapping("/api/permission_status")
    public ResponseEntity<?> updatePermissionStatus(
            @Valid @RequestBody UpdatePermissionStatusRequest request
    ) {
        ExtendedPermissionStatusDto dto = permissionStatusService.updatePermissionStatus(request);
        ExtendedPermissionStatusResponse response = statusMapper.toDetailedResponse(dto);

        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/api/permission_status")
    public ResponseEntity<?> removePermissionStatuses(
            @Valid @RequestBody DeletePermissionStatusRequest request
    ) {
        permissionStatusService.deletePermissionStatuses(request);
        return ResponseEntity.noContent().build();
    }

}
