package ru.ompec.iam.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.ompec.iam.domain.EmployeeStatus;
import ru.ompec.iam.dto.response.ExtendedEmployeeStatusDto;
import ru.ompec.iam.mapper.EmployeeStatusMapper;
import ru.ompec.iam.payload.employeestatus.*;
import ru.ompec.iam.service.EmployeeStatusService;

import javax.validation.Valid;
import java.net.URI;
import java.util.Set;
import java.util.UUID;

@RestController
public class EmployeeStatusController {

    private final EmployeeStatusMapper statusMapper;
    private final EmployeeStatusService statusService;

    @Autowired
    public EmployeeStatusController(
            EmployeeStatusMapper statusMapper,
            EmployeeStatusService statusService
    ) {
        this.statusMapper = statusMapper;
        this.statusService = statusService;
    }

    @PostMapping("/api/employee_status")
    public ResponseEntity<?> createEmployeeStatus(
            @Valid @RequestBody CreateEmployeeStatusRequest request
    ) {
        EmployeeStatus status = statusService.createEmployeeStatus(request);
        URI location = URI.create("/api/employee_status/" + status.getId());
        EmployeeStatusResponse response = statusMapper.toResponse(status);

        return ResponseEntity.created(location).body(response);
    }

    @GetMapping("/api/employee_status/{id}")
    public ResponseEntity<?> getEmployeeStatus(
            @PathVariable("id") UUID id
    ) {
        ExtendedEmployeeStatusDto dto = statusService.findByIdDetailed(id);
        ExtendedEmployeeStatusResponse response = statusMapper.toDetailedResponse(dto);

        return ResponseEntity.ok(response);
    }

    @GetMapping("/api/employee_statuses")
    public ResponseEntity<?> getEmployeeStatuses() {
        Set<ExtendedEmployeeStatusDto> dtos = statusService.findAllDetailed();
        Set<ExtendedEmployeeStatusResponse> responses = statusMapper.toDetailedResponses(dtos);
        EmployeeStatusesResponse response = new EmployeeStatusesResponse(responses);

        return ResponseEntity.ok(response);
    }

    @PutMapping("/api/employee_status")
    public ResponseEntity<?> updateEmployeeStatus(
        @Valid @RequestBody UpdateEmployeeStatusRequest request
    ) {
        ExtendedEmployeeStatusDto dto = statusService.updateEmployeeStatus(request);
        ExtendedEmployeeStatusResponse response = statusMapper.toDetailedResponse(dto);

        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/api/employee_status")
    public ResponseEntity<?> removeEmployeeStatuses(
        @Valid @RequestBody DeleteEmployeeStatusRequest request
    ) {
        statusService.deleteEmployeeStatuses(request);
        return ResponseEntity.noContent().build();
    }


}
