package ru.ompec.iam.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.ompec.iam.domain.User;
import ru.ompec.iam.mapper.UserMapper;
import ru.ompec.iam.payload.user.*;
import ru.ompec.iam.security.CurrentUser;
import ru.ompec.iam.security.UserPrincipal;
import ru.ompec.iam.service.UserService;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.UUID;


@RestController
public class UserController {

    private final UserMapper userMapper;
    private final UserService userService;

    @Autowired
    public UserController(
            UserMapper userMapper,
            UserService userService
    ) {
        this.userMapper = userMapper;
        this.userService = userService;
    }

    @GetMapping("/api/user/me")
    public ResponseEntity<?> getUserInfo(@CurrentUser UserPrincipal userPrincipal) {
        User user = userService.findUserById(userPrincipal.getId());
        UserResponse response = userMapper.toResponse(user);

        return ResponseEntity.ok(response);
    }

    @PostMapping("/api/user")
    public ResponseEntity<?> createUser(
            @Valid @RequestBody CreateUserRequest request
    ) {
        User user = userService.createUser(request);
        URI location = URI.create("/api/user/" + user.getId());
        UserResponse response = userMapper.toResponse(user);

        return ResponseEntity.created(location).body(response);
    }

    @DeleteMapping("/api/user/{id}")
    public ResponseEntity<?> deleteUser(
            @PathVariable("id") UUID id
    ) {
        userService.deleteUser(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/api/user/{id}")
    public ResponseEntity<?> getUserById(
            @PathVariable("id") UUID id
    ) {
        User user = userService.findUserById(id);
        UserResponse response = userMapper.toResponse(user);

        return ResponseEntity.ok(response);
    }

    @GetMapping("/api/users")
    public ResponseEntity<?> getAllUsers() {
        List<User> users = userService.findAllUsers();
        List<UserResponse> responses = userMapper.toResponses(users);

        return ResponseEntity.ok(new UsersResponse(responses));
    }

    @PutMapping("/api/user")
    public ResponseEntity<?> updateUser(
            @Valid @RequestBody UpdateUserRequest request
    ) {
        User user = userService.updateUser(request);
        UserResponse response = userMapper.toResponse(user);

        return ResponseEntity.ok(response);
    }

    @PutMapping("/api/user/change_password")
    public ResponseEntity<?> updateUserPassword(
            @Valid @RequestBody UpdateUserPasswordRequest request
    ) {
        User user = userService.updateUserPassword(request);
        UserResponse response = userMapper.toResponse(user);

        return ResponseEntity.ok(response);
    }

}
