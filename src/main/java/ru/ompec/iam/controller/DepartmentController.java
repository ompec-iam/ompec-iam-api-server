package ru.ompec.iam.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.ompec.iam.domain.Department;
import ru.ompec.iam.dto.response.ExtendedDepartmentDto;
import ru.ompec.iam.mapper.DepartmentMapper;
import ru.ompec.iam.payload.department.*;
import ru.ompec.iam.service.DepartmentService;

import javax.validation.Valid;
import java.net.URI;
import java.util.Set;
import java.util.UUID;

@RestController
public class DepartmentController {

    private final DepartmentService departmentService;
    private final DepartmentMapper departmentMapper;

    @Autowired
    public DepartmentController(DepartmentService departmentService,
                                DepartmentMapper departmentMapper) {
        this.departmentService = departmentService;
        this.departmentMapper = departmentMapper;
    }

    @PostMapping("/api/department")
    public ResponseEntity<?> createDepartment(@Valid @RequestBody CreateDepartmentRequest request) {
        Department department = departmentService.createDepartment(request);
        URI location = URI.create("/api/department/" + department.getId());
        DepartmentResponse response = departmentMapper.toResponse(department);

        return ResponseEntity.created(location).body(response);
    }

    @GetMapping("/api/department/{id}")
    public ResponseEntity<?> getDepartment(@PathVariable("id") UUID id) {
        ExtendedDepartmentDto dto = departmentService.findDepartmentByIdDetailed(id);
        ExtendedDepartmentResponse response = departmentMapper.toDetailedResponse(dto);

        return ResponseEntity.ok(response);
    }

    @GetMapping("/api/departments")
    public ResponseEntity<?> getDepartments() {
        Set<ExtendedDepartmentDto> dtos = departmentService.findDepartmentsDetailed();
        Set<ExtendedDepartmentResponse> responses = departmentMapper.toDetailedResponses(dtos);
        DepartmentsResponse response = new DepartmentsResponse(responses);

        return ResponseEntity.ok(response);
    }

    @PutMapping("/api/department")
    public ResponseEntity<?> updateDepartment(
            @Valid @RequestBody UpdateDepartmentRequest request
    ) {
        ExtendedDepartmentDto dto = departmentService.updateDepartment(request);
        ExtendedDepartmentResponse response = departmentMapper.toDetailedResponse(dto);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/api/department")
    public ResponseEntity<?> deleteDepartments(
            @Valid @RequestBody DeleteDepartmentsRequest request
    ) {
        departmentService.deleteDepartments(request);
        return ResponseEntity.noContent().build();
    }

}
