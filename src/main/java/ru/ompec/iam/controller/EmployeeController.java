package ru.ompec.iam.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.ompec.iam.domain.Employee;
import ru.ompec.iam.pagination.specification.EmployeeSpecificationsBuilder;
import ru.ompec.iam.payload.employee.DeleteEmployeesRequest;
import ru.ompec.iam.payload.employee.EmployeeResponse;
import ru.ompec.iam.mapper.EmployeeMapper;
import ru.ompec.iam.payload.employee.CreateEmployeeRequest;
import ru.ompec.iam.payload.employee.UpdateEmployeeRequest;
import ru.ompec.iam.service.EmployeeService;


import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;
import java.util.UUID;

@RestController
public class EmployeeController {

    private final EmployeeService employeeService;
    private final EmployeeMapper employeeMapper;

    @Autowired
    public EmployeeController(EmployeeService employeeService,
                              EmployeeMapper employeeMapper) {
        this.employeeService = employeeService;
        this.employeeMapper = employeeMapper;
    }

    @PostMapping("/api/employee")
    public ResponseEntity<?> createEmployee(@Valid @RequestBody CreateEmployeeRequest request) {
        Employee employee = employeeService.createEmployee(request);
        URI location = URI.create("/api/employee/" + employee.getId());
        EmployeeResponse response = employeeMapper.toResponse(employee);

        return ResponseEntity.created(location).body(response);
    }

    @GetMapping("/api/employee/{id}")
    public ResponseEntity<?> getEmployeeById(@PathVariable("id") String employeeId) {
        Employee employee = employeeService.getEmployeeById(UUID.fromString(employeeId));
        EmployeeResponse response = employeeMapper.toResponse(employee);

        return ResponseEntity.ok(response);
    }

    @GetMapping("/api/v2/employees")
    public ResponseEntity<?> filterEmployees(
            @RequestParam(value = "filter", required = false) Optional<String> filter,
            @PageableDefault(
                    size = 10,
                    sort = {
                            "firstName",
                            "lastName",
                            "patronymic",
                            "position"
                    },
                    direction = Sort.Direction.ASC
            ) Pageable pageable
    ) {
        EmployeeSpecificationsBuilder builder = new EmployeeSpecificationsBuilder();

        if(filter.isPresent()) {
            String[] params = filter.get().split(",");

            for(String param : params) {
                String[] payload = param.split(":");
                builder.with(payload[0], payload[1]);
            }
        }

        Specification<Employee> spec = builder.build();

        Page<EmployeeResponse> page = employeeService.findAll(spec, pageable)
                                                     .map(employeeMapper::toResponse);
        return ResponseEntity.ok(page);
    }

    @GetMapping("/api/employees")
    public ResponseEntity<?> getEmployees(
            @RequestParam(
                    required = false,
                    name = "departmentId"
            ) Optional<UUID> departmentId,
            @PageableDefault(
                    size = 10,
                    sort = {
                            "firstName",
                            "lastName",
                            "patronymic",
                            "position"
                    },
                    direction = Sort.Direction.ASC
            ) Pageable pageable
    ) {
        Page<EmployeeResponse> page = employeeService.getEmployeesByDepartment(departmentId, pageable).map(employeeMapper::toResponse);
        return ResponseEntity.ok(page);
    }

    @DeleteMapping("/api/employee/{id}")
    public ResponseEntity<?> removeEmployee(@PathVariable("id") UUID employeeId) {
        employeeService.removeEmployee(employeeId);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("/api/employee")
    public ResponseEntity<?> removeMultipleEmployees(@Valid @RequestBody DeleteEmployeesRequest request) {
        employeeService.removeEmployees(request.getEmployees());
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/api/employee")
    public ResponseEntity<?> updateEmployee(@Valid @RequestBody UpdateEmployeeRequest request) {
        Employee employee = employeeService.updateEmployee(request);
        EmployeeResponse response = employeeMapper.toResponse(employee);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/api/employee/upload_photo/{id}")
    public ResponseEntity<?> uploadEmployeePhoto(
            @RequestParam("photo") MultipartFile photo,
            @PathVariable("id") UUID employeeId
    ) {
        employeeService.saveEmployeePhoto(photo, employeeId);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/api/employee/delete_photo/{id}")
    public ResponseEntity<?> deleteEmployeePhoto(
            @PathVariable("id") UUID employeeId
    ) {
        employeeService.deleteEmployeePhoto(employeeId);
        return ResponseEntity.noContent().build();
    }

}
