package ru.ompec.iam.payload.credential;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import ru.ompec.iam.security.UserPrincipal;


import java.util.UUID;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CredentialResponse {

    private UUID id;
    private String login;
    private String password;

    @JsonGetter("password")
    public String getPasswordJSON() {
        UserPrincipal principal   = (UserPrincipal) SecurityContextHolder.getContext()
                                                                         .getAuthentication()
                                                                         .getPrincipal();
        return principal.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN")) ? password : "********";
    }

}
