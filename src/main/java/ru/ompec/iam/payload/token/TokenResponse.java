package ru.ompec.iam.payload.token;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.UUID;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TokenResponse {

    private UUID id;
    private UUID userId;
    private String ipAddress;
    private String deviceInfo;

}
