package ru.ompec.iam.payload.employee;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.UUID;

@Data
public class DeleteEmployeesRequest {

    @NotEmpty(message = "{delete_employees.list_ids.not_null}")
    private List<UUID> employees;

}
