package ru.ompec.iam.payload.employee;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import ru.ompec.iam.payload.department.DepartmentResponse;
import ru.ompec.iam.payload.employeestatus.EmployeeStatusResponse;
import ru.ompec.iam.payload.permission.PermissionResponse;

import java.util.List;
import java.util.UUID;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EmployeeResponse {

    private UUID id;
    private String firstName;
    private String lastName;
    private String patronymic;
    private String position;
    private String phoneNumber;
    private String photoFileName;
    private DepartmentResponse department;
    private EmployeeStatusResponse status;
    private List<PermissionResponse> permissions;


}
