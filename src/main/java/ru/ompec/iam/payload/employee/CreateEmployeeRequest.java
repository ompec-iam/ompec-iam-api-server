package ru.ompec.iam.payload.employee;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.ompec.iam.validation.*;
import ru.ompec.iam.validation.constraints.common.OnlyCharacters;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@Setter(AccessLevel.NONE)
@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@GroupSequence({
        CreateEmployeeRequest.class,
        FirstOrder.class,
        SecondOrder.class,
        ThirdOrder.class,
        FourthOrder.class,
        FifthOrder.class,
        SixthOrder.class,
        SeventhOrder.class,
        EighthOrder.class,
        NinthOrder.class,
        TenthOrder.class,
        EleventhOrder.class,
        TwelfthOrder.class,
        ThirteenthOrder.class
})
public class CreateEmployeeRequest {

    @NotBlank(message = "{employee.firstname.not.empty}", groups = FirstOrder.class)
    @Size(message = "{employee.firstname.max_length}", max = 50, groups = SecondOrder.class)
    @OnlyCharacters(message = "{employee.firstname.only_characters}", groups = ThirdOrder.class)
    private final String firstName;

    @NotBlank(message = "{employee.lastname.not.empty}", groups = FourthOrder.class)
    @Size(message = "{employee.lastname.max_length}", max = 50, groups = FifthOrder.class)
    @OnlyCharacters(message = "{employee.lastname.only_characters}", groups = SixthOrder.class)
    private final String lastName;

    @Size(message = "{employee.patronymic.max_length}", max = 50, groups = SeventhOrder.class)
    @OnlyCharacters(message = "{employee.patronymic.only_characters}", groups = EighthOrder.class)
    private final String patronymic;

    @NotBlank(message = "{employee.position.not_empty}", groups = NinthOrder.class)
    @Size(message = "{employee.position.max_length}", max = 250, groups = TenthOrder.class)
    private final String position;

    @Size(message = "{employee.phonenumber.max_length}", min = 11, max = 11, groups = EleventhOrder.class)
    private final String phoneNumber;

    @NotNull(message = "{employee.departmentid.not_empty}", groups = TwelfthOrder.class)
    private final UUID departmentId;

    @NotNull(message = "{employee.status_id.not_empty}", groups = ThirteenthOrder.class)
    private final UUID employeeStatusId;

    @JsonCreator
    public CreateEmployeeRequest(
            @JsonProperty(value = "firstName", required = true) final String firstName,
            @JsonProperty(value = "lastName", required = true) final String lastName,
            @JsonProperty(value = "patronymic") final String patronymic,
            @JsonProperty(value = "position", required = true) final String position,
            @JsonProperty(value = "phoneNumber") final String phoneNumber,
            @JsonProperty(value = "departmentId", required = true) final UUID departmentId,
            @JsonProperty(value = "employeeStatusId", required = true) final UUID employeeStatusId
    ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.patronymic = patronymic;
        this.position = position;
        this.phoneNumber = phoneNumber;
        this.departmentId = departmentId;
        this.employeeStatusId = employeeStatusId;
    }

}
