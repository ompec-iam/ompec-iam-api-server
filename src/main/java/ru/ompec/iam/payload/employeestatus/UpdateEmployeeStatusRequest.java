package ru.ompec.iam.payload.employeestatus;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.ompec.iam.validation.FirstOrder;
import ru.ompec.iam.validation.SecondOrder;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Setter(AccessLevel.NONE)
@Getter
@ToString
@GroupSequence({
        UpdateEmployeeStatusRequest.class,
        FirstOrder.class,
        SecondOrder.class,
})
public class UpdateEmployeeStatusRequest {

    @NotNull(message = "{employee_status.id.not_empty}", groups = FirstOrder.class)
    private final UUID employeeStatusId;

    @NotBlank(message = "{employee_status.color.not_empty}", groups = SecondOrder.class)
    private final String color;

    @JsonCreator
    public UpdateEmployeeStatusRequest(
        @JsonProperty(value = "employeeStatusId", required = true) final UUID employeeStatusId,
        @JsonProperty(value = "color", required = true) final String color
    ) {
        this.employeeStatusId = employeeStatusId;
        this.color = color;
    }

}
