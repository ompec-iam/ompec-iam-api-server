package ru.ompec.iam.payload.employeestatus;

import lombok.Data;
import ru.ompec.iam.validation.FirstOrder;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.UUID;

@Data
@GroupSequence({
        DeleteEmployeeStatusRequest.class,
        FirstOrder.class
})
public class DeleteEmployeeStatusRequest {

    @NotEmpty(message = "{delete_employee_status.ids.not_empty}", groups = FirstOrder.class)
    private List<UUID> employeeStatuses;

}
