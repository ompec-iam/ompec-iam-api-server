package ru.ompec.iam.payload.permissionstatus;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.ompec.iam.validation.FirstOrder;
import ru.ompec.iam.validation.SecondOrder;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Setter(AccessLevel.NONE)
@Getter
@ToString
@GroupSequence({
        UpdatePermissionStatusRequest.class,
        FirstOrder.class,
        SecondOrder.class,
})
public class UpdatePermissionStatusRequest {

    @NotNull(message = "{permission_status.id.not_empty}", groups = FirstOrder.class)
    private final UUID permissionStatusId;

    @NotBlank(message = "{permission_status.color.not_empty}", groups = SecondOrder.class)
    private final String color;

    @JsonCreator
    public UpdatePermissionStatusRequest(
        @JsonProperty(value = "permissionStatusId", required = true) final UUID permissionStatusId,
        @JsonProperty(value = "color", required = true) final String color
    ) {
        this.permissionStatusId = permissionStatusId;
        this.color = color;
    }

}
