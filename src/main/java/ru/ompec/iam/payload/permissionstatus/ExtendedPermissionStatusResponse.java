package ru.ompec.iam.payload.permissionstatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExtendedPermissionStatusResponse {

    private UUID id;
    private String status;
    private String color;
    private Long permissionCount;

}
