package ru.ompec.iam.payload.permissionstatus;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.ompec.iam.validation.FirstOrder;
import ru.ompec.iam.validation.SecondOrder;
import ru.ompec.iam.validation.ThirdOrder;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@ToString
@GroupSequence({
        CreatePermissionStatusRequest.class,
        FirstOrder.class,
        SecondOrder.class,
        ThirdOrder.class
})
public class CreatePermissionStatusRequest {

    @Setter(AccessLevel.NONE)
    @NotBlank(message = "{permission_status.not_empty}", groups = FirstOrder.class)
    @Size(message = "{permission_status.length}", max = 40, groups = SecondOrder.class)
    private final String status;

    @Setter(AccessLevel.NONE)
    @NotBlank(message = "{permission_status.color.not_empty}", groups = ThirdOrder.class)
    private final String color;

    @JsonCreator
    public CreatePermissionStatusRequest(
            @JsonProperty(value = "status", required = true) final String status,
            @JsonProperty(value = "color", required = true) final String color
    ) {
        this.status = status;
        this.color = color;
    }

}
