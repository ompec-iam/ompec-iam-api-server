package ru.ompec.iam.payload.permissionstatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Set;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PermissionStatusesResponse {

    private Set<ExtendedPermissionStatusResponse> statuses;

}
