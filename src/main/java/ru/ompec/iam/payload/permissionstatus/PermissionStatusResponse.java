package ru.ompec.iam.payload.permissionstatus;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.UUID;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PermissionStatusResponse {

    private UUID id;
    private String color;
    private String status;

}
