package ru.ompec.iam.payload.permissionstatus;

import lombok.Data;
import ru.ompec.iam.validation.FirstOrder;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.UUID;

@Data
@GroupSequence({
        DeletePermissionStatusRequest.class,
        FirstOrder.class
})
public class DeletePermissionStatusRequest {

    @NotEmpty(message = "{delete_permission_status.ids.not_empty}", groups = FirstOrder.class)
    private List<UUID> permissionStatuses;
}
