package ru.ompec.iam.payload.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.ompec.iam.validation.*;
import ru.ompec.iam.validation.constraints.common.EmployeePassword;
import ru.ompec.iam.validation.constraints.common.NotSpace;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@Setter(AccessLevel.NONE)
@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@GroupSequence({
        UpdateUserPasswordRequest.class,
        FirstOrder.class,
        SecondOrder.class,
        ThirdOrder.class,
        FourthOrder.class,
        FifthOrder.class
})
public class UpdateUserPasswordRequest {

    @NotNull(message = "{user.user_id.not_empty}", groups = FirstOrder.class)
    private final UUID userId;

    @NotBlank(message = "{user.password.not_empty}", groups = SecondOrder.class)
    @NotSpace(message = "{user.password.not_space}", groups = ThirdOrder.class)
    @Size(min = 8, max = 40, message = "{user.password.length}", groups = FourthOrder.class)
    @EmployeePassword(message = "{user.password.pattern}", groups = FifthOrder.class)
    private final String password;

    @JsonCreator
    public UpdateUserPasswordRequest(
            @JsonProperty(value = "userId", required = true) final UUID userId,
            @JsonProperty(value = "password", required = true) final String password
    ) {
        this.userId = userId;
        this.password = password;
    }

}
