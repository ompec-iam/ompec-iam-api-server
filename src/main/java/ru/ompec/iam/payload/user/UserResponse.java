package ru.ompec.iam.payload.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import ru.ompec.iam.payload.role.RoleResponse;
import ru.ompec.iam.payload.token.TokenResponse;

import java.util.List;
import java.util.UUID;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserResponse {

    private UUID id;
    private String username;
    private boolean isActive;
    private List<RoleResponse> roles;
    private List<TokenResponse> authTokens;

}
