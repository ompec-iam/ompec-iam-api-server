package ru.ompec.iam.payload.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.ompec.iam.validation.*;
import ru.ompec.iam.validation.constraints.common.EmployeePassword;
import ru.ompec.iam.validation.constraints.common.NotSpace;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.UUID;

@Setter(AccessLevel.NONE)
@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@GroupSequence({
        CreateUserRequest.class,
        FirstOrder.class,
        SecondOrder.class,
        ThirdOrder.class,
        FourthOrder.class,
        FifthOrder.class,
        SixthOrder.class,
        SeventhOrder.class,
        EighthOrder.class,
        NinthOrder.class
})
public class CreateUserRequest {

    @NotBlank(message = "{user.login.not_empty}", groups = FirstOrder.class)
    @NotSpace(message = "{user.login.not_space}", groups = SecondOrder.class)
    @Size(message = "{user.login.length}", min = 3, max = 20, groups = ThirdOrder.class)
    @Pattern(regexp = "[A-Za-z0-9_]+", message = "{user.login.pattern}", groups = FourthOrder.class)
    private String login;

    @NotBlank(message = "{user.password.not_empty}", groups = FifthOrder.class)
    @NotSpace(message = "{user.password.not_space}", groups = SixthOrder.class)
    @Size(min = 8, max = 40, message = "{user.password.length}", groups = SeventhOrder.class)
    @EmployeePassword(message = "{user.password.pattern}", groups = EighthOrder.class)
    private String password;

    @NotNull(message = "{user.role_id.not_empty}", groups = NinthOrder.class)
    private UUID roleId;
    private Boolean isActive;

    @JsonCreator
    public CreateUserRequest(
        @JsonProperty(value = "login", required = true) final String login,
        @JsonProperty(value = "password", required = true) final String password,
        @JsonProperty(value = "roleId", required = true) final UUID roleId,
        @JsonProperty(value = "isActive", required = true) final Boolean isActive
    ) {
        this.login = login;
        this.password = password;
        this.roleId = roleId;
        this.isActive = isActive;
    }

}
