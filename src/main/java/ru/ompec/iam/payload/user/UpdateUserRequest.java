package ru.ompec.iam.payload.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.ompec.iam.validation.*;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Setter(AccessLevel.NONE)
@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
@GroupSequence({
        UpdateUserRequest.class,
        FirstOrder.class,
        SecondOrder.class
})
public class UpdateUserRequest {

    @NotNull(message = "{user.user_id.not_empty}", groups = FirstOrder.class)
    private final UUID userId;
    @NotNull(message = "{user.role_id.not_empty}", groups = SecondOrder.class)
    private final UUID roleId;
    private final Boolean isActive;

    @JsonCreator
    public UpdateUserRequest(
            @JsonProperty(value = "userId", required = true) final UUID userId,
            @JsonProperty(value = "roleId", required = true) final UUID roleId,
            @JsonProperty(value = "isActive", required = true) final Boolean isActive
    ) {
        this.userId = userId;
        this.roleId = roleId;
        this.isActive = isActive;
    }

}
