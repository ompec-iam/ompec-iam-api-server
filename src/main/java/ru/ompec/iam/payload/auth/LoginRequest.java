package ru.ompec.iam.payload.auth;

import lombok.Data;

@Data
public class LoginRequest {

    private String username;
    private String password;

}
