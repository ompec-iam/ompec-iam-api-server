package ru.ompec.iam.payload.resource;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExtendedResourceResponse {

    private UUID id;
    private String title;
    private Long permissionCount;

}
