package ru.ompec.iam.payload.resource;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.ompec.iam.validation.FirstOrder;
import ru.ompec.iam.validation.SecondOrder;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Setter(AccessLevel.NONE)
@ToString
@GroupSequence({
        UpdateResourceRequest.class,
        FirstOrder.class,
        SecondOrder.class
})
public class UpdateResourceRequest {

    @NotBlank(message = "{resource.title.not.empty}", groups = FirstOrder.class)
    private final String title;

    @NotNull(message = "{resource.id.not_null}", groups = SecondOrder.class)
    private final UUID resourceId;

    @JsonCreator
    public UpdateResourceRequest(
            @JsonProperty(value = "title", required = true) final String title,
            @JsonProperty(value = "resourceId", required = true) final UUID resourceId
    ) {
        this.title = title;
        this.resourceId = resourceId;
    }

}
