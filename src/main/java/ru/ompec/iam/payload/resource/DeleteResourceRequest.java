package ru.ompec.iam.payload.resource;

import lombok.Data;
import ru.ompec.iam.validation.FirstOrder;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.UUID;

@Data
@GroupSequence({
        DeleteResourceRequest.class,
        FirstOrder.class
})
public class DeleteResourceRequest {

    @NotEmpty(message = "{delete_resources.list_ids.not_empty}", groups = FirstOrder.class)
    private List<UUID> resources;

}
