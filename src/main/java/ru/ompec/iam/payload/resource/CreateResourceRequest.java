package ru.ompec.iam.payload.resource;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.ompec.iam.validation.FirstOrder;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotBlank;

@Getter
@ToString
@GroupSequence({ CreateResourceRequest.class, FirstOrder.class })
public class CreateResourceRequest {

    @Setter(AccessLevel.NONE)
    @NotBlank(message = "{resource.title.not.empty}", groups = FirstOrder.class)
    private final String title;

    @JsonCreator
    public CreateResourceRequest(@JsonProperty(value = "title", required = true) final String title) {
        this.title = title;
    }

}
