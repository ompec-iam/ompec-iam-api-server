package ru.ompec.iam.payload.resource;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.UUID;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResourceResponse {

    private UUID id;
    private String title;

}
