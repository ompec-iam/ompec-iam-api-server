package ru.ompec.iam.payload.department;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import ru.ompec.iam.validation.FirstOrder;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotBlank;

@Getter
@ToString
@GroupSequence({ CreateDepartmentRequest.class, FirstOrder.class })
public class CreateDepartmentRequest {

    @Setter(AccessLevel.NONE)
    @NotBlank(message = "{department.title.not.empty}", groups = FirstOrder.class)
    private final String title;

    @JsonCreator
    public CreateDepartmentRequest(@JsonProperty(value = "title", required = true) final String title) {
        this.title = title;
    }

}
