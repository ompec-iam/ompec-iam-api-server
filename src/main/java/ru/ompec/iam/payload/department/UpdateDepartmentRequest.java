package ru.ompec.iam.payload.department;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.ompec.iam.validation.FirstOrder;
import ru.ompec.iam.validation.SecondOrder;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Getter
@Setter(AccessLevel.NONE)
@ToString
@GroupSequence({
        UpdateDepartmentRequest.class,
        FirstOrder.class,
        SecondOrder.class
})
public class UpdateDepartmentRequest {

    @NotNull(message = "{department.department_id.not_empty}", groups = FirstOrder.class)
    private final UUID departmentId;

    @NotBlank(message = "{department.title.not.empty}", groups = FirstOrder.class)
    private final String title;

    @JsonCreator
    public UpdateDepartmentRequest(
            @JsonProperty(value = "departmentId", required = true) final UUID departmentId,
            @JsonProperty(value = "title", required = true) final String title
    ) {
        this.departmentId = departmentId;
        this.title = title;
    }

}
