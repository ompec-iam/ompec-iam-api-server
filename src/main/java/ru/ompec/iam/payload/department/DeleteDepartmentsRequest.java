package ru.ompec.iam.payload.department;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.UUID;

@Data
public class DeleteDepartmentsRequest {

    @NotEmpty(message = "{delete_departments.list_ids.not_null}")
    private List<UUID> departments;

}
