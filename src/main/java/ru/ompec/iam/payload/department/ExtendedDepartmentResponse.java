package ru.ompec.iam.payload.department;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ExtendedDepartmentResponse {

    private UUID id;
    private String title;
    private Long employeeCount;

}
