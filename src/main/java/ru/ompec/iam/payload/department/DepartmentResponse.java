package ru.ompec.iam.payload.department;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.UUID;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DepartmentResponse {

    private UUID id;
    private String title;

}
