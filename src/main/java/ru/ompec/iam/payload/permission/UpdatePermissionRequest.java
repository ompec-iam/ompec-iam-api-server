package ru.ompec.iam.payload.permission;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;
import ru.ompec.iam.validation.*;
import ru.ompec.iam.validation.constraints.common.EmployeePassword;
import ru.ompec.iam.validation.constraints.common.NotSpace;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@Getter
@ToString
@GroupSequence({
        UpdatePermissionRequest.class,
        FirstOrder.class,
        SecondOrder.class,
        ThirdOrder.class,
        FourthOrder.class,
        FifthOrder.class,
        SixthOrder.class
})
public class UpdatePermissionRequest {

    @NotNull(message = "{permission.permission_status_id.not_empty}", groups = FirstOrder.class)
    private final UUID statusId;

    // @NotBlank(message = "{permission.password.not_empty}", groups = SecondOrder.class)
    @NotSpace(message = "{permission.password.not_space}", groups = ThirdOrder.class)
    @Size(min = 8, max = 40, message = "{permission.password.length}", groups = FourthOrder.class)
    @EmployeePassword(message = "{permission.password.pattern}", groups = FifthOrder.class)
    private final String password;

    @NotNull(message = "{permission.permission_id.not_null}", groups = SixthOrder.class)
    private final UUID permissionId;

    @JsonCreator
    public UpdatePermissionRequest(
            @JsonProperty(value = "statusId", required = true) final UUID statusId,
            @JsonProperty(value = "password", required = false) final String password,
            @JsonProperty(value = "permissionId", required = true) final UUID permissionId
    ) {
        this.statusId = statusId;
        this.password = password;
        this.permissionId = permissionId;
    }

}
