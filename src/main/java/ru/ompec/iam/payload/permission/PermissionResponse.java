package ru.ompec.iam.payload.permission;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import ru.ompec.iam.payload.credential.CredentialResponse;
import ru.ompec.iam.payload.permissionstatus.PermissionStatusResponse;
import ru.ompec.iam.payload.resource.ResourceResponse;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PermissionResponse {

    private UUID id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime creationDate;
    private PermissionStatusResponse status;
    private ResourceResponse resource;
    private UUID employeeId;
    private CredentialResponse credential;

}
