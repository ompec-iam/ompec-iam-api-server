package ru.ompec.iam.payload.permission;

import lombok.Data;
import ru.ompec.iam.validation.*;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@Data
@GroupSequence({
        DeletePermissionRequest.class,
        FirstOrder.class
})
public class DeletePermissionRequest {

    @NotNull(message = "{delete_permission.permissions.not_null}", groups = FirstOrder.class)
    private List<UUID> permissions;

}
