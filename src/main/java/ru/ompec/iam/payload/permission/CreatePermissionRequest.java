package ru.ompec.iam.payload.permission;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.ToString;
import ru.ompec.iam.validation.*;
import ru.ompec.iam.validation.constraints.common.EmployeePassword;
import ru.ompec.iam.validation.constraints.common.NotSpace;

import javax.validation.GroupSequence;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.UUID;

@Getter
@ToString
@GroupSequence({ CreatePermissionRequest.class, FirstOrder.class, SecondOrder.class,
        ThirdOrder.class, FourthOrder.class, FifthOrder.class, SixthOrder.class,
        SeventhOrder.class, EighthOrder.class, NinthOrder.class, TenthOrder.class,
        EleventhOrder.class
})
public class CreatePermissionRequest {

    @NotNull(message = "{permission.permission_status_id.not_empty}", groups = FirstOrder.class)
    private final UUID permissionStatusId;

    @NotNull(message = "{permission.resource_id.not_empty}", groups = SecondOrder.class)
    private final UUID resourceId;

    @NotNull(message = "{permission.employee_id.not_empty}", groups = ThirdOrder.class)
    private final UUID employeeId;

    @NotBlank(message = "{permission.login.not_empty}", groups = FourthOrder.class)
    @NotSpace(message = "{permission.login.not_space}", groups = FifthOrder.class)
    @Size(min = 5, max = 50, message = "{permission.login.length}", groups = SixthOrder.class)
    @Pattern(regexp = "[A-Za-z0-9_]+", message = "{permission.login.pattern}", groups = SeventhOrder.class)
    private final String login;

    // @NotBlank(message = "{permission.password.not_empty}", groups = EighthOrder.class)
    @NotSpace(message = "{permission.password.not_space}", groups = NinthOrder.class)
    @Size(min = 8, max = 40, message = "{permission.password.length}", groups = TenthOrder.class)
    @EmployeePassword(message = "{permission.password.pattern}", groups = EleventhOrder.class)
    private final String password;

    @JsonCreator
    public CreatePermissionRequest(
            @JsonProperty(value = "permissionStatusId", required = true) final UUID permissionStatusId,
            @JsonProperty(value = "resourceId", required = true) final UUID resourceId,
            @JsonProperty(value = "employeeId", required = true) final UUID employeeId,
            @JsonProperty(value = "login", required = true) final String login,
            @JsonProperty(value = "password", required = false) final String password
    ) {
        this.permissionStatusId = permissionStatusId;
        this.resourceId = resourceId;
        this.employeeId = employeeId;
        this.login = login;
        this.password = password;
    }

}
