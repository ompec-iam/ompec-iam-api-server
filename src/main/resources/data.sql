insert into rle(id, name) values('4869ce38-d1b2-4fe1-a60a-27c4485d90c0', 'ROLE_ADMIN');
insert into rle(id, name) values('5b29db3f-83c5-4320-b4d8-19f305398c76', 'ROLE_USER');

insert into usr(id, username, password_hash, is_active) values('ad303c4f-bde2-4e69-9018-bab9cfe0075e', 'anya', '$2a$10$/T3UuN2F3mNdRuM3b.8ZbuvtXxt9jrgTyblu/KtrFb8J5KqLWXi7i', true);
insert into usr(id, username, password_hash, is_active) values('e5f4d2c1-600b-419c-8e48-eec2b204b7a5', 'kirill', '$2a$10$/T3UuN2F3mNdRuM3b.8ZbuvtXxt9jrgTyblu/KtrFb8J5KqLWXi7i', true);

insert into user_role(user_id, role_id) values('ad303c4f-bde2-4e69-9018-bab9cfe0075e', '4869ce38-d1b2-4fe1-a60a-27c4485d90c0');
insert into user_role(user_id, role_id) values('e5f4d2c1-600b-419c-8e48-eec2b204b7a5', '5b29db3f-83c5-4320-b4d8-19f305398c76');

insert into department(id, title) values('7bad0611-15d0-4d63-9462-756ce083d10a', N'Нефтехимическое отделение');
insert into department(id, title) values('0ca4c17b-c87f-4eaf-86b8-281aeeb185f5', N'Отделение информационных технологий');
insert into department(id, title) values('739659ee-fec3-4dfd-b95b-003b78547fff', N'Отделение экономики и права');

insert into resource(id ,title) values('db3fa029-345d-498a-b3a2-4061c1bebd3a', N'АMoodle');
insert into resource(id ,title) values('bc2ade42-1632-46ff-91d8-640089fc2492', N'БСайт');
insert into resource(id ,title) values('714cda86-4571-467f-b2dd-205470a7e4da', N'ВЕСИА');
insert into resource(id ,title) values('54ac6483-57ea-403c-ad73-a5fb5cda045d', N'Г1С: Бухгалтерия');

insert into employee_status(id, status, color) values ('153bbe49-2980-4c59-9e87-c28bb5b32213', N'Работает', N'#0DE009FF');
insert into employee_status(id, status, color) values ('dad16b7a-7421-4b02-9c85-1f4b5e40b2e9', N'Уволен', N'#E01710FF');

insert into permission_status(id, status, color) values('fc133077-0f79-4d4d-9bcf-8e0604ea8ba1', N'Действует', N'#0DE009FF');
insert into permission_status(id, status, color) values('69ad0929-1988-49aa-ab0c-f84a24f35ba7', N'Не действует', N'#E01710FF');

insert into employee(id, first_name, last_name, patronymic, phone_number, position, department_id, employee_status_id) values ('6a44637f-878a-4e36-b12f-1ef3a8349003', N'Анна', N'Кулакова', N'Ивановна', '89605526262', N'Преподаватель', '7bad0611-15d0-4d63-9462-756ce083d10a', '153bbe49-2980-4c59-9e87-c28bb5b32213');
insert into employee(id, first_name, last_name, patronymic, phone_number, position, department_id, employee_status_id) values ('3594976d-2791-44fe-af0a-fb98c797a881', N'Кирилл', N'Баженов', N'Сергеевич', '89508872536', N'Преподаватель', '7bad0611-15d0-4d63-9462-756ce083d10a', '153bbe49-2980-4c59-9e87-c28bb5b32213');

insert into employee(id, first_name, last_name, patronymic, phone_number, position, department_id, employee_status_id) values ('87bc98d3-26a5-4887-a1eb-957081861a7f', N'Кирилл', N'Авдеев', N'Владиславович', '89506187976', N'Преподаватель', '7bad0611-15d0-4d63-9462-756ce083d10a', '153bbe49-2980-4c59-9e87-c28bb5b32213');
insert into employee(id, first_name, last_name, patronymic, phone_number, position, department_id, employee_status_id) values ('c4773356-e5e3-4b5e-9f02-15e36345fae3', N'Елисей', N'Андреев', N'Даниилович', '89509263110', N'Преподаватель', '7bad0611-15d0-4d63-9462-756ce083d10a', '153bbe49-2980-4c59-9e87-c28bb5b32213');
insert into employee(id, first_name, last_name, patronymic, phone_number, position, department_id, employee_status_id) values ('d8ef792e-d48e-4cbd-ad72-5193abd2f5d2', N'Руслан', N'Артемов', N'Кириллович', '89508444520', N'Преподаватель', '7bad0611-15d0-4d63-9462-756ce083d10a', '153bbe49-2980-4c59-9e87-c28bb5b32213');
insert into employee(id, first_name, last_name, patronymic, phone_number, position, department_id, employee_status_id) values ('a2f7f99e-de9c-434f-a373-323f1fbbc958', N'Сергей', N'Артемов', N'Сергеевич', '89501770356', N'Преподаватель', '7bad0611-15d0-4d63-9462-756ce083d10a', '153bbe49-2980-4c59-9e87-c28bb5b32213');
insert into employee(id, first_name, last_name, patronymic, phone_number, position, department_id, employee_status_id) values ('e2282998-acbd-4fa0-8f96-9fbb9625d047', N'Михаил', N'Архипов', N'Михайлович', '89506476232', N'Преподаватель', '0ca4c17b-c87f-4eaf-86b8-281aeeb185f5', '153bbe49-2980-4c59-9e87-c28bb5b32213');
insert into employee(id, first_name, last_name, patronymic, phone_number, position, department_id, employee_status_id) values ('721ad5c7-2592-45c0-90b5-a7bb7841aed4', N'Матвей', N'Белов', N'Никитич', '89506958161', N'Преподаватель', '0ca4c17b-c87f-4eaf-86b8-281aeeb185f5', '153bbe49-2980-4c59-9e87-c28bb5b32213');
insert into employee(id, first_name, last_name, patronymic, phone_number, position, department_id, employee_status_id) values ('2cb56f03-2a9f-47ab-810f-384398746df1', N'Тимофей', N'Бочаров', N'Константинович', '89500646955', N'Преподаватель', '0ca4c17b-c87f-4eaf-86b8-281aeeb185f5', '153bbe49-2980-4c59-9e87-c28bb5b32213');
insert into employee(id, first_name, last_name, patronymic, phone_number, position, department_id, employee_status_id) values ('5019ddf1-3ec6-4c21-afa9-9cad8009d1df', N'Александр', N'Васильев', N'Сергеевич', '89503307877', N'Преподаватель', '0ca4c17b-c87f-4eaf-86b8-281aeeb185f5', '153bbe49-2980-4c59-9e87-c28bb5b32213');
insert into employee(id, first_name, last_name, patronymic, phone_number, position, department_id, employee_status_id) values ('4378c198-52f5-48a5-b820-c82731c21824', N'Михаил', N'Виноградов', N'Михайлович', '89501523845', N'Преподаватель', '0ca4c17b-c87f-4eaf-86b8-281aeeb185f5', '153bbe49-2980-4c59-9e87-c28bb5b32213');
insert into employee(id, first_name, last_name, patronymic, phone_number, position, department_id, employee_status_id) values ('3a1bdc3b-03b3-4441-acd4-624dbdb644b7', N'Тимофей', N'Виноградов', N'Тимофеевич', '89504036646', N'Преподаватель', '0ca4c17b-c87f-4eaf-86b8-281aeeb185f5', '153bbe49-2980-4c59-9e87-c28bb5b32213');
insert into employee(id, first_name, last_name, patronymic, phone_number, position, department_id, employee_status_id) values ('4c17db88-125b-49b5-9c76-5e885f316c19', N'Марк', N'Владимиров', N'Робертович', '89501140600', N'Преподаватель', '0ca4c17b-c87f-4eaf-86b8-281aeeb185f5', '153bbe49-2980-4c59-9e87-c28bb5b32213');
insert into employee(id, first_name, last_name, patronymic, phone_number, position, department_id, employee_status_id) values ('8f00ad9d-1ea3-4b24-a53c-8efd3ddd2248', N'Артём', N'Воронов', N'Маркович', '89503162847', N'Преподаватель', '739659ee-fec3-4dfd-b95b-003b78547fff', 'dad16b7a-7421-4b02-9c85-1f4b5e40b2e9');
insert into employee(id, first_name, last_name, patronymic, phone_number, position, department_id, employee_status_id) values ('cabfead2-1e9a-4d5b-aa18-5d72c1b072cf', N'Михаил', N'Гаврилов', N'Александрович', '89509937493', N'Преподаватель', '739659ee-fec3-4dfd-b95b-003b78547fff', 'dad16b7a-7421-4b02-9c85-1f4b5e40b2e9');
insert into employee(id, first_name, last_name, patronymic, phone_number, position, department_id, employee_status_id) values ('f7f56b1b-10cc-4953-961e-f685f68e7f1b', N'Александр', N'Герасимов', N'Иванович', '89509656114', N'Преподаватель', '739659ee-fec3-4dfd-b95b-003b78547fff', '153bbe49-2980-4c59-9e87-c28bb5b32213');
insert into employee(id, first_name, last_name, patronymic, phone_number, position, department_id, employee_status_id) values ('8d2ed37c-8200-4129-b17a-180378ce288c', N'Виктор', N'Гришин', N'Маркович', '89503839849', N'Преподаватель', '739659ee-fec3-4dfd-b95b-003b78547fff', '153bbe49-2980-4c59-9e87-c28bb5b32213');
insert into employee(id, first_name, last_name, patronymic, phone_number, position, department_id, employee_status_id) values ('2711d047-e164-4f89-b886-2e8e49a674e5', N'Иван', N'Дубровин', N'Миронович', '89505534914', N'Преподаватель', '739659ee-fec3-4dfd-b95b-003b78547fff', '153bbe49-2980-4c59-9e87-c28bb5b32213');
insert into employee(id, first_name, last_name, patronymic, phone_number, position, department_id, employee_status_id) values ('3c8b0ab9-49b6-495a-9d3c-d24189b6c61b', N'Ян', N'Егоров', N'Богданович', '89501766705', N'Преподаватель', '739659ee-fec3-4dfd-b95b-003b78547fff', '153bbe49-2980-4c59-9e87-c28bb5b32213');
insert into employee(id, first_name, last_name, patronymic, phone_number, position, department_id, employee_status_id) values ('0eda77e6-e14a-4ff6-930d-ef00e00b7b87', N'Ян', N'Ершов', N'Николаевич', '89506048281', N'Преподаватель', '739659ee-fec3-4dfd-b95b-003b78547fff', '153bbe49-2980-4c59-9e87-c28bb5b32213');

insert into credential(id, login, password) values('778742a6-d464-4516-812d-29191ef116af', 'anita_shlyapa', 'mRT.8uKo_');
insert into credential(id, login, password) values('2b781088-7063-4540-a915-17ec6998726b', 'anita_shlyapa', 'mRT.7mKo_');
insert into credential(id, login, password) values('cd30e83d-dbef-488f-bf7a-54a497901022', 'anita_shlyapa', 'mRT.9lKo_');
insert into credential(id, login, password) values('f677cabc-fd89-41c2-9401-4589ad4f0e6e', 'anita_shlyapa', 'mRT.21kjUKo_');

insert into credential(id, login, password) values('e2ad3dea-9cb3-4d0e-9e11-a8f905486cc6', 'kirill_bazhenov', 'mLS.21kjUKo_');
insert into credential(id, login, password) values('486cb4fa-90c3-4fa3-a5c0-a013e71e383e', 'kirill_bazhenov', 'rLS.21fjUKo_');
insert into credential(id, login, password) values('f185eb90-030a-4f74-98ad-982a8ed355cd', 'kirill_bazhenov', 'qmLS.298jUKo_');
insert into credential(id, login, password) values('ab48d11d-4d90-40cb-85cd-f93f9ba633be', 'kirill_bazhenov', 'mLSD.21kMuKo_');

insert into permission(id, employee_id, resource_id, permission_status_id) values('391e6c8f-6ca4-4a76-b813-6bf0625b25f5', '6a44637f-878a-4e36-b12f-1ef3a8349003', 'db3fa029-345d-498a-b3a2-4061c1bebd3a', 'fc133077-0f79-4d4d-9bcf-8e0604ea8ba1');
insert into permission(id, employee_id, resource_id, permission_status_id) values('a37d514b-497a-47d1-afa9-75144c45a18b', '6a44637f-878a-4e36-b12f-1ef3a8349003', 'bc2ade42-1632-46ff-91d8-640089fc2492', 'fc133077-0f79-4d4d-9bcf-8e0604ea8ba1');
insert into permission(id, employee_id, resource_id, permission_status_id) values('a2bc0dfc-bb0c-42e9-b45b-a436f61991b2', '6a44637f-878a-4e36-b12f-1ef3a8349003', '714cda86-4571-467f-b2dd-205470a7e4da', 'fc133077-0f79-4d4d-9bcf-8e0604ea8ba1');
insert into permission(id, employee_id, resource_id, permission_status_id) values('bc470200-1d4b-4f4c-84eb-4ee949470ae5', '6a44637f-878a-4e36-b12f-1ef3a8349003', '54ac6483-57ea-403c-ad73-a5fb5cda045d', '69ad0929-1988-49aa-ab0c-f84a24f35ba7');

insert into permission(id, employee_id, resource_id, permission_status_id) values('460a6cd3-5e1d-4838-8aeb-705adb6934d9', '3594976d-2791-44fe-af0a-fb98c797a881', 'db3fa029-345d-498a-b3a2-4061c1bebd3a', 'fc133077-0f79-4d4d-9bcf-8e0604ea8ba1');
insert into permission(id, employee_id, resource_id, permission_status_id) values('f38cb7e4-b6b8-4dc7-897c-540984f69657', '3594976d-2791-44fe-af0a-fb98c797a881', 'bc2ade42-1632-46ff-91d8-640089fc2492', 'fc133077-0f79-4d4d-9bcf-8e0604ea8ba1');
insert into permission(id, employee_id, resource_id, permission_status_id) values('2f1f069e-e535-4a4f-a407-5ef706151fc4', '3594976d-2791-44fe-af0a-fb98c797a881', '714cda86-4571-467f-b2dd-205470a7e4da', '69ad0929-1988-49aa-ab0c-f84a24f35ba7');
insert into permission(id, employee_id, resource_id, permission_status_id) values('cd7feaf7-8318-4cd5-a008-b025d53cdfdf', '3594976d-2791-44fe-af0a-fb98c797a881', '54ac6483-57ea-403c-ad73-a5fb5cda045d', 'fc133077-0f79-4d4d-9bcf-8e0604ea8ba1');

insert into permission_credential(credential_id, permission_id) values('778742a6-d464-4516-812d-29191ef116af', '391e6c8f-6ca4-4a76-b813-6bf0625b25f5');
insert into permission_credential(credential_id, permission_id) values('2b781088-7063-4540-a915-17ec6998726b', 'a37d514b-497a-47d1-afa9-75144c45a18b');
insert into permission_credential(credential_id, permission_id) values('cd30e83d-dbef-488f-bf7a-54a497901022', 'a2bc0dfc-bb0c-42e9-b45b-a436f61991b2');
insert into permission_credential(credential_id, permission_id) values('f677cabc-fd89-41c2-9401-4589ad4f0e6e', 'bc470200-1d4b-4f4c-84eb-4ee949470ae5');

insert into permission_credential(credential_id, permission_id) values('e2ad3dea-9cb3-4d0e-9e11-a8f905486cc6', '460a6cd3-5e1d-4838-8aeb-705adb6934d9');
insert into permission_credential(credential_id, permission_id) values('486cb4fa-90c3-4fa3-a5c0-a013e71e383e', 'f38cb7e4-b6b8-4dc7-897c-540984f69657');
insert into permission_credential(credential_id, permission_id) values('f185eb90-030a-4f74-98ad-982a8ed355cd', '2f1f069e-e535-4a4f-a407-5ef706151fc4');
insert into permission_credential(credential_id, permission_id) values('ab48d11d-4d90-40cb-85cd-f93f9ba633be', 'cd7feaf7-8318-4cd5-a008-b025d53cdfdf');
