create table credential (
                            id varchar(36) not null,
                            login varchar(35),
                            password varchar(255),
                            primary key (id)
);

create table department (
                            id varchar(36) not null,
                            title varchar(250),
                            primary key (id)
);

create table employee (
                          id varchar(36) not null,
                          first_name varchar(50) not null,
                          last_name varchar(50) not null,
                          patronymic varchar(250),
                          phone_number varchar(12),
                          photo_file_name varchar(255),
                          position varchar(250),
                          department_id varchar(36),
                          employee_status_id varchar(36),
                          primary key (id)
);

create table employee_status (
                                 id varchar(36) not null,
                                 color varchar(255),
                                 status varchar(40) not null,
                                 primary key (id)
);

create table permission (
                            id varchar(36) not null,
                            creation_date timestamp,
                            employee_id varchar(36),
                            resource_id varchar(36),
                            permission_status_id varchar(36),
                            primary key (id)
);

create table permission_credential (
                                       credential_id varchar(36),
                                       permission_id varchar(36) not null,
                                       primary key (permission_id)
);

create table permission_status (
                                   id varchar(36) not null,
                                   color varchar(255),
                                   status varchar(40) not null,
                                   primary key (id)
);

create table resource (
                          id varchar(36) not null,
                          title varchar(255),
                          primary key (id)
);

create table rle (
                     id varchar(36) not null,
                     name varchar(255),
                     primary key (id)
);

create table token (
                       id varchar(36) not null,
                       device_info varchar(500),
                       ip_address varchar(255),
                       token varchar(2048) not null,
                       user_id varchar(36),
                       primary key (id)
);

create table user_role (
                           user_id varchar(36) not null,
                           role_id varchar(36) not null,
                           primary key (user_id, role_id)
);

create table usr (
                     id varchar(36) not null,
                     is_active boolean not null,
                     password_hash varchar(255) not null,
                     username varchar(30) not null,
                     primary key (id)
);

alter table if exists department
add constraint UK_rtauc18issrejt60w59k9cda1 unique (title);

alter table if exists employee
add constraint UK_if2qx9bhvigig71puh5sow65g unique (phone_number);

alter table if exists employee_status
add constraint UK_epdi9o8ix4wr6gckmyr8j11ou unique (status);

alter table if exists permission_status
add constraint UK_eo6woom280xeg0bvvqsv0s1e5 unique (status);

alter table if exists resource
add constraint UK_hl5vkgx20fef2y5e2298cvml7 unique (title);

alter table if exists usr
add constraint UK_dfui7gxngrgwn9ewee3ogtgym unique (username);

alter table if exists employee
add constraint FKbejtwvg9bxus2mffsm3swj3u9
       foreign key (department_id)
       references department;

alter table if exists employee
add constraint FK4704ko2smpmsrtx3v0lkc4ovk
       foreign key (employee_status_id)
       references employee_status;

alter table if exists permission
add constraint FKiodh4e1kienhcxygd6u1vtl2h
       foreign key (employee_id)
       references employee;

alter table if exists permission
add constraint FKsuaohbjcgyxlm25g9r7ih52rx
       foreign key (resource_id)
       references resource;

alter table if exists permission
add constraint FK1takbd5oom720ulrahd9hfjif
       foreign key (permission_status_id)
       references permission_status;

alter table if exists permission_credential
add constraint FK4uxiroywketi6g97dieadnpcm
       foreign key (credential_id)
       references credential;

alter table if exists permission_credential
    add constraint FK6iq0s42hvcjmdjrhepsyrscjd
       foreign key (permission_id)
       references permission;

alter table if exists token
add constraint FKssf1kt08wvjrqg5x50facgn4g
       foreign key (user_id)
       references usr;

alter table if exists user_role add constraint FK90kqpq1yh56uwne6l7kjafr44
       foreign key (role_id)
       references rle;

alter table if exists user_role add constraint FKfpm8swft53ulq2hl11yplpr5
       foreign key (user_id)
       references usr;